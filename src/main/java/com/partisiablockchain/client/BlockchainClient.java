package com.partisiablockchain.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.client.shards.ShardManager;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.client.web.JerseyWebClient;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.AccountState;
import com.partisiablockchain.dto.AvlStateValue;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.dto.Event;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.dto.Feature;
import com.partisiablockchain.dto.IncomingTransaction;
import com.partisiablockchain.dto.JarState;
import com.partisiablockchain.dto.TransactionPointer;
import com.partisiablockchain.dto.traversal.TraversePath;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.rest.RestResources;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.GenericType;
import java.net.URI;
import java.util.HexFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * A client that supports communicating with the PBC blockchain.
 *
 * <p>The underlying communication uses a web-client communicating with a <i>reader node</i> using
 * HTTP and REST.
 */
public final class BlockchainClient implements BlockchainContractClient {

  /** Base path for blockchain resource. */
  public static final String BLOCKCHAIN_BASE_PATH = "/blockchain";

  /** Path to get an account. */
  public static final String ACCOUNT_GET = "/account/{address}";

  /** Path to get account info. */
  public static final String ACCOUNT_INFO = "/chain/accounts/{address}";

  /** Path to get chain id. */
  public static final String CHAIN_ID_GET = "/chainId";

  /** Path to get the state of a contract. */
  public static final String CONTRACT_STATE_GET = "/contracts/{address}";

  /** Path to get the next n avl state entries of a contract. */
  public static final String CONTRACT_STATE_AVL_GET_VALUE =
      "/contracts/{address}/avl/{treeId}/{key}";

  /** Path to get the next n avl state entries of a contract. */
  public static final String CONTRACT_STATE_AVL_GET_NEXT_N =
      "/contracts/{address}/avl/{treeId}/next/{key}";

  /** Path to get the next n avl state entries of a contract. */
  public static final String CONTRACT_STATE_AVL_GET_FIRST_N =
      "/contracts/{address}/avl/{treeId}/next";

  /** Path to get the diff of an avl state between two block times. */
  public static final String CONTRACT_STATE_AVL_DIFF_NEXT =
      "/contracts/{address}/avl/{treeId}/diff/{blockTime1}/{blockTime2}/{key}";

  /** Path to get the diff of an avl state between two block times. */
  public static final String CONTRACT_STATE_AVL_DIFF_FIRST =
      "/contracts/{address}/avl/{treeId}/diff/{blockTime1}/{blockTime2}";

  /** Path to traverse the state of a contract. */
  public static final String TRAVERSE_CONTRACT_STATE = "/contracts/{address}";

  /** Path to get a transaction. */
  public static final String TRANSACTION_GET = "/transaction/{transactionHash}";

  /** Path to put a transaction. */
  public static final String TRANSACTION_PUT = "/transaction";

  /** Path to get the latest block. */
  public static final String LATEST_BLOCK_GET = "/blocks/latest";

  /** Path to get the latest block before an optional UTC time. */
  public static final String BLOCKTIME_GET = "/blocks/latestBlockTime";

  /** Path to get a block using the hash of the block. */
  public static final String BLOCK_BY_HASH_GET = "/blocks/{blockHash}";

  /** Path to get all transactions in a block using the hash of the block. */
  public static final String BLOCK_BY_HASH_TRANSACTIONS_GET = "/blocks/{blockHash}/transactions";

  /** Path to get a block using the block time of the block. */
  public static final String BLOCK_BY_NUMBER_GET = "/blocks/blockTime/{blockTime}";

  /** Path to get a feature. */
  public static final String FEATURES_GET = "/features/{feature}";

  /** Path to traverse the global account plugin. */
  public static final String TRAVERSE_ACCOUNT_GLOBAL = "/accountPlugin/global";

  /** Path to traverse the local account plugin. */
  public static final String TRAVERSE_ACCOUNT_LOCAL = "/accountPlugin/local";

  /** Path to traverse the global consensus plugin. */
  public static final String TRAVERSE_CONSENSUS_GLOBAL = "/consensusPlugin/global";

  /** Path to traverse the local consensus plugin. */
  public static final String TRAVERSE_CONSENSUS_LOCAL = "/consensusPlugin/local";

  /** Path to put an event transaction. */
  public static final String EVENT_PUT = "/event";

  /** Path to get an event transaction. */
  public static final String EVENT_GET = "/event/{eventHash}";

  /** Path to get the consensus jar. */
  public static final String CONSENSUS_JAR_GET = "/consensusJar";

  /** The base of the url of the targeted PBC. */
  private final String baseUrl;

  /** The shards of the blockchain. */
  private final ShardManager shardManager;

  /** A web client that communicates via HTTP. */
  private final WebClient webClient;

  /**
   * Creates a new blockchain client with a supplied web-client.
   *
   * @param baseUrl The base of the url of the targeted PBC. Must satisfy {@link #parseBaseUrl}.
   * @param numberOfShards The number of shards at the targeted PBC.
   * @param webClient A web client that communicates via HTTP.
   */
  BlockchainClient(String baseUrl, int numberOfShards, WebClient webClient) {
    this.baseUrl = baseUrl;
    this.shardManager = new ShardManager(numberOfShards);
    this.webClient = webClient;
  }

  /**
   * Parses, validates and normalizes a given URL (as {@link String}). Specifies the invariants that
   * all {@link BlockchainClient#baseUrl}s must observe.
   *
   * <p>In human terms:
   *
   * <ul>
   *   <li>The base url must use the {@code http} or {@code https} protocol.
   *   <li>It must specify a hostname, and can include a port number and a path.
   *   <li>A single trailing slash ({@code /}) in the path will be removed, if present.
   * </ul>
   *
   * @param urlString URL to parse, validate, and normalize.
   * @return Normalized URL as String.
   */
  public static String parseBaseUrl(String urlString) {
    final URI url = URI.create(urlString);

    // Validate url
    if (!"http".equals(url.getScheme()) && !"https".equals(url.getScheme())) {
      throw new IllegalArgumentException(
          "Base url '%s' was not of the expected format: scheme must be either http or https"
              .formatted(url));
    }

    if (url.getQuery() != null) {
      throw new IllegalArgumentException(
          "Base url '%s' was not of the expected format: must not contain a query parameters"
              .formatted(url));
    }

    if (url.getFragment() != null) {
      throw new IllegalArgumentException(
          "Base url '%s' was not of the expected format: must not contain fragments"
              .formatted(url));
    }

    // Strip trailing slash
    return urlString.endsWith("/") ? urlString.substring(0, urlString.length() - 1) : urlString;
  }

  /**
   * Create a new blockchain client to interact with the PBC.
   *
   * @param baseUrl The base of the url of the targeted PBC. Must satisfy {@link #parseBaseUrl}.
   * @param numberOfShards The number of shards at the targeted PBC.
   * @return A new blockchain client.
   */
  public static BlockchainClient create(String baseUrl, int numberOfShards) {
    Client client =
        ClientBuilder.newBuilder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .withConfig(new ResourceConfig().register(RestResources.DEFAULT))
            .build();
    JerseyWebClient webClient = new JerseyWebClient(client);
    return new BlockchainClient(parseBaseUrl(baseUrl), numberOfShards, webClient);
  }

  /**
   * Get the shard manager that keeps track of the shards of the blockchain.
   *
   * @return The shard manager
   */
  public ShardManager getShardManager() {
    return shardManager;
  }

  @Override
  public AccountState getAccountState(BlockchainAddress address) {
    return webClient.get(
        finalizeRestEndpoint(shardManager.getShard(address), ACCOUNT_GET),
        AccountState.class,
        Map.of("address", address.writeAsString()));
  }

  /**
   * Retrieve the block of the provided hash.
   *
   * @param shardId The name of the shard to interact with.
   * @param hash The hash of the block to get.
   * @return The block dto of the provided hash.
   */
  public BlockState getBlockByHash(ShardId shardId, Hash hash) {
    return webClient.get(
        finalizeRestEndpoint(shardId, BLOCK_BY_HASH_GET),
        BlockState.class,
        Map.of("blockHash", hash.toString()));
  }

  /**
   * Retrieve the block having the provided number.
   *
   * @param shardId The name of the shard to interact with.
   * @param number The number of the block.
   * @return The block having the provided number.
   */
  public BlockState getBlockByNumber(ShardId shardId, long number) {
    return webClient.get(
        finalizeRestEndpoint(shardId, BLOCK_BY_NUMBER_GET),
        BlockState.class,
        Map.of("blockTime", String.valueOf(number)));
  }

  @Override
  public ChainId getChainId() {
    return webClient.get(
        finalizeRestEndpoint(shardManager.getGovernanceShard(), CHAIN_ID_GET), ChainId.class);
  }

  /**
   * Get the consensus plugin jar for the targeted PBC.
   *
   * @return The consensus plugin jar data object.
   */
  public JarState getConsensusPluginJar() {
    return webClient.get(
        finalizeRestEndpoint(shardManager.getGovernanceShard(), CONSENSUS_JAR_GET), JarState.class);
  }

  /**
   * Get the state of a smart contract, including state serialized by the contract itself.
   *
   * @param address The address of the contract to get.
   * @param stateOutput The format to use for the state serialized by the contract itself.
   * @param blockTime The block time to get the state at. If -1 gets the latest block time.
   * @return The state of the contract.
   */
  public ContractState getContractState(
      BlockchainAddress address, StateOutput stateOutput, long blockTime) {
    return webClient.get(
        finalizeRestEndpoint(shardManager.getShard(address), CONTRACT_STATE_GET),
        Map.of(
            StateOutput.STATE_OUTPUT,
            stateOutput.name().toLowerCase(Locale.getDefault()),
            "blockTime",
            blockTime),
        ContractState.class,
        Map.of("address", address.writeAsString()));
  }

  /**
   * Get the state of a smart contract, including state serialized by the contract itself.
   *
   * @param address The address of the contract to get.
   * @param stateOutput The format to use for the state serialized by the contract itself.
   * @return The state of the contract.
   */
  public ContractState getContractState(BlockchainAddress address, StateOutput stateOutput) {
    return getContractState(address, stateOutput, -1);
  }

  @Override
  public ContractState getContractState(BlockchainAddress address) {
    return getContractState(address, StateOutput.JSON);
  }

  /**
   * Get the next n entries of state Avl Tree based from a given key (Excluding the key itself). If
   * key is null, gets the first n entries.
   *
   * @param address The address of the contract to get.
   * @param treeId The id of the Avl Tree to get.
   * @param key The key to base the search from. If null, gets the first n entries.
   * @param n The number of entries to get.
   * @param blockTime The block time to get the entries at. If -1 gets the latest block time.
   * @return List of n key, value pairs. May return fewer than n values if fewer matching items
   *     exist in the tree.
   */
  public List<Map.Entry<byte[], byte[]>> getContractStateAvlNextN(
      BlockchainAddress address, int treeId, byte[] key, int n, long blockTime) {
    if (key == null) {
      return webClient.get(
          finalizeRestEndpoint(shardManager.getShard(address), CONTRACT_STATE_AVL_GET_FIRST_N),
          Map.of("n", n, "blockTime", blockTime),
          new GenericType<>() {},
          Map.of("address", address.writeAsString(), "treeId", treeId));
    } else {
      return webClient.get(
          finalizeRestEndpoint(shardManager.getShard(address), CONTRACT_STATE_AVL_GET_NEXT_N),
          Map.of("n", n, "blockTime", blockTime),
          new GenericType<>() {},
          Map.of(
              "address",
              address.writeAsString(),
              "treeId",
              treeId,
              "key",
              HexFormat.of().formatHex(key)));
    }
  }

  /**
   * Get the next n entries of state Avl Tree based from a given key (Excluding the key itself). If
   * key is null, gets the first n entries.
   *
   * @param address The address of the contract to get.
   * @param treeId The id of the Avl Tree to get.
   * @param key The key to base the search from. If null, gets the first n entries.
   * @param n The number of entries to get.
   * @return List of n key, value pairs. May return fewer than n values if fewer matching items
   *     exist in the tree.
   */
  public List<Map.Entry<byte[], byte[]>> getContractStateAvlNextN(
      BlockchainAddress address, int treeId, byte[] key, int n) {
    return getContractStateAvlNextN(address, treeId, key, n, -1);
  }

  /**
   * Get the modified keys of an Avl Tree between two block times. The search based from the given
   * key (Excluding the key itself) and can return at most n elements. If key is null, it searches
   * from the beginning.
   *
   * @param address The address of the contract to get.
   * @param treeId The id of the Avl Tree to get.
   * @param blockTime1 the first block times.
   * @param blockTime2 the second block times.
   * @param key The key to base the search from. If null, gets the first n entries.
   * @param n The number of entries to get.
   * @return List of n key, value pairs. May return fewer than n values if fewer matching items
   *     exist in the tree.
   */
  public List<byte[]> getContractStateAvlModifiedKeys(
      BlockchainAddress address, int treeId, long blockTime1, long blockTime2, byte[] key, int n) {
    if (key == null) {
      return webClient.get(
          finalizeRestEndpoint(shardManager.getShard(address), CONTRACT_STATE_AVL_DIFF_FIRST),
          Map.of("n", n),
          new GenericType<>() {},
          Map.of(
              "address",
              address.writeAsString(),
              "treeId",
              treeId,
              "blockTime1",
              blockTime1,
              "blockTime2",
              blockTime2));
    } else {
      return webClient.get(
          finalizeRestEndpoint(shardManager.getShard(address), CONTRACT_STATE_AVL_DIFF_NEXT),
          Map.of("n", n),
          new GenericType<>() {},
          Map.of(
              "address",
              address.writeAsString(),
              "treeId",
              treeId,
              "blockTime1",
              blockTime1,
              "blockTime2",
              blockTime2,
              "key",
              HexFormat.of().formatHex(key)));
    }
  }

  /**
   * Get the value corresponding to the given key from the state Avl Tree.
   *
   * @param address The address of the contract to get.
   * @param treeId The id of the Avl Tree to get.
   * @param key The key.
   * @param blockTime The block time to get the entries at. If -1 gets the latest block time.
   * @return The corresponding value.
   */
  public AvlStateValue getContractStateAvlValue(
      BlockchainAddress address, int treeId, byte[] key, long blockTime) {
    return webClient.get(
        finalizeRestEndpoint(shardManager.getShard(address), CONTRACT_STATE_AVL_GET_VALUE),
        Map.of("blockTime", blockTime),
        AvlStateValue.class,
        Map.of(
            "address",
            address.writeAsString(),
            "treeId",
            treeId,
            "key",
            HexFormat.of().formatHex(key)));
  }

  /**
   * Get the value corresponding to the given key from the state Avl Tree.
   *
   * @param address The address of the contract to get.
   * @param treeId The id of the Avl Tree to get.
   * @param key The key.
   * @return The corresponding value.
   */
  public AvlStateValue getContractStateAvlValue(BlockchainAddress address, int treeId, byte[] key) {
    return getContractStateAvlValue(address, treeId, key, -1);
  }

  /**
   * Retrieve an event by its identifier.
   *
   * @param shardId The name of the shard to interact with.
   * @param identifier The identifier of the event to retrieve.
   * @return The event matching the provided identifier.
   */
  public Event getEvent(ShardId shardId, Hash identifier) {
    return webClient.get(
        finalizeRestEndpoint(shardId, EVENT_GET),
        Event.class,
        Map.of("eventHash", identifier.toString()));
  }

  /**
   * Retrieve the value of a feature from the targeted PBC.
   *
   * @param feature The key of the feature.
   * @return The value of the feature or null if not set.
   */
  public Feature getFeature(String feature) {
    return webClient.get(
        finalizeRestEndpoint(shardManager.getGovernanceShard(), FEATURES_GET),
        Feature.class,
        Map.of("feature", feature));
  }

  @Override
  public BlockState getLatestBlock(ShardId shardId) {
    return webClient.get(finalizeRestEndpoint(shardId, LATEST_BLOCK_GET), BlockState.class);
  }

  /**
   * Retrieve the latest block before an optional UTC time.
   *
   * @param shardId The name of the shard to interact with.
   * @param utcTime the utc time to find the block before that.
   * @return the block just before the supplied time.
   */
  public Long getLatestBlockBeforeTime(ShardId shardId, long utcTime) {
    return webClient.get(
        finalizeRestEndpoint(shardId, BLOCKTIME_GET),
        Map.of("utcTime", utcTime),
        Long.class,
        Map.of());
  }

  @Override
  public ExecutedTransaction getTransaction(ShardId shardId, Hash hash) {
    return webClient.get(
        finalizeRestEndpoint(shardId, TRANSACTION_GET),
        ExecutedTransaction.class,
        Map.of("transactionHash", hash.toString()));
  }

  /**
   * Retrieve all executed transactions for a block.
   *
   * @param shardId The name of the shard to interact with.
   * @param hash The hash of the block to get.
   * @return all executed transactions for the block
   */
  public List<ExecutedTransaction> getTransactionsForBlock(ShardId shardId, Hash hash) {
    return webClient.get(
        finalizeRestEndpoint(shardId, BLOCK_BY_HASH_TRANSACTIONS_GET),
        new GenericType<>() {},
        Map.of("blockHash", hash.toString()));
  }

  /**
   * Sends a possibly unsigned transaction to the targeted PBC.
   *
   * @param shardId The name of the shard to interact with.
   * @param event The transaction to send.
   */
  public void putEvent(ShardId shardId, IncomingTransaction event) {
    webClient.put(finalizeRestEndpoint(shardId, EVENT_PUT), event);
  }

  @Override
  public TransactionPointer putTransaction(SignedTransaction signedTransaction) {
    ShardId senderShard = shardManager.getShard(signedTransaction.recoverSender());
    IncomingTransaction dto =
        new IncomingTransaction(SafeDataOutputStream.serialize(signedTransaction::write));
    webClient.put(finalizeRestEndpoint(senderShard, TRANSACTION_PUT), dto);
    return new TransactionPointer(signedTransaction.identifier(), senderShard.name());
  }

  /**
   * Retrieve sub-state of contract from address by traversing its fields.
   *
   * @param shardId The name of the shard to interact with.
   * @param address the address of the contract.
   * @param path the dto containing the path to the desired sub-state.
   * @return the sub-state as a JsonNode.
   */
  public JsonNode traverseContractState(
      ShardId shardId, BlockchainAddress address, TraversePath path) {
    return webClient.post(
        finalizeRestEndpoint(shardId, TRAVERSE_CONTRACT_STATE),
        path,
        JsonNode.class,
        Map.of("address", address.writeAsString()));
  }

  /**
   * Retrieve sub-state of global account plugin by traversing its fields.
   *
   * @param path The dto containing the path to the desired sub-state.
   * @return The sub-state as a JsonNode.
   */
  public JsonNode traverseGlobalAccountPlugin(TraversePath path) {
    return webClient.post(
        finalizeRestEndpoint(shardManager.getGovernanceShard(), TRAVERSE_ACCOUNT_GLOBAL),
        path,
        JsonNode.class);
  }

  /**
   * Get the information for a given account.
   *
   * @param address the blockchain address of the account.
   * @return the account info as a JsonNode.
   */
  public JsonNode getAccountInfo(BlockchainAddress address) {
    return webClient.get(
        baseUrl + ACCOUNT_INFO, JsonNode.class, Map.of("address", address.writeAsString()));
  }

  /**
   * Retrieve sub-state of global consensus plugin by traversing its fields.
   *
   * @param path The dto containing the path to the desired sub-state.
   * @return The sub-state as a JsonNode.
   */
  public JsonNode traverseGlobalConsensusPlugin(TraversePath path) {
    return webClient.post(
        finalizeRestEndpoint(shardManager.getGovernanceShard(), TRAVERSE_CONSENSUS_GLOBAL),
        path,
        JsonNode.class);
  }

  /**
   * Retrieve sub-state of local account plugin by traversing its fields.
   *
   * @param shardId The name of the shard to interact with.
   * @param path The dto containing the path to the desired sub-state.
   * @return The sub-state as a JsonNode.
   */
  public JsonNode traverseLocalAccountPlugin(ShardId shardId, TraversePath path) {
    return webClient.post(
        finalizeRestEndpoint(shardId, TRAVERSE_ACCOUNT_LOCAL), path, JsonNode.class);
  }

  /**
   * Retrieve sub-state of local consensus plugin by traversing its fields.
   *
   * @param shardId The name of the shard to interact with.
   * @param path The dto containing the path to the desired sub-state.
   * @return The sub-state as a JsonNode.
   */
  public JsonNode traverseLocalConsensusPlugin(ShardId shardId, TraversePath path) {
    return webClient.post(
        finalizeRestEndpoint(shardId, TRAVERSE_CONSENSUS_LOCAL), path, JsonNode.class);
  }

  /**
   * Creates the remaining part of the REST endpoint of the targeted PBC based on the provided shard
   * name and path.
   *
   * @param shardId Shard to create endpoint URL for. Not nullable.
   * @param path Subpath to create endpoint URL for. Not nullable.
   * @return URL to REST endpoint.
   */
  String finalizeRestEndpoint(ShardId shardId, String path) {
    final String shardPath = shardId.name() == null ? "" : "/shards/" + shardId.name();
    return baseUrl + shardPath + BLOCKCHAIN_BASE_PATH + path;
  }

  /** The formats in which the contract state can be requested. */
  public enum StateOutput {

    /** The state serialized by the contract itself is not included. */
    NONE,

    /** Format the contract state as JSON. */
    JSON,

    /** Format serialized contract state as binary data. */
    BINARY,

    /** Format serialized state and AvlTrees as binary data. */
    AVL_BINARY;

    /** The query parameter used to request a specific format. */
    static final String STATE_OUTPUT = "stateOutput";
  }
}
