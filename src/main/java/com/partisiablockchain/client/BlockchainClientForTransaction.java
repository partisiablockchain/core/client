package com.partisiablockchain.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.dto.TransactionPointer;

/**
 * A client for getting the information to create a transaction, and send it after creation and
 * signing.
 */
public interface BlockchainClientForTransaction {

  /**
   * Get the chain ID for the running chain.
   *
   * @return the chain ID data object.
   */
  ChainId getChainId();

  /**
   * Get the current nonce to use when signing a transaction for the given account.
   *
   * @param blockchainAddress the address to get the nonce for.
   * @return the current nonce to use for signing.
   */
  long getNonce(BlockchainAddress blockchainAddress);

  /**
   * Retrieve an executed transaction from the blockchain.
   *
   * @param shardId The name of the shard to interact with.
   * @param identifier The transaction hash.
   * @return The retrieved transaction.
   */
  ExecutedTransaction getTransaction(ShardId shardId, Hash identifier);

  /**
   * Get the production time from the latest created block of a specific shard.
   *
   * @param shardId the id of the shard.
   * @return the production time of the latest created block.
   */
  long getLatestProductionTime(ShardId shardId);

  /**
   * Send a signed transaction to the chain and return a pointer to the transaction.
   *
   * @param signedTransaction the transaction to send.
   * @return a pointer to the transaction on chain.
   */
  TransactionPointer putTransaction(SignedTransaction signedTransaction);
}
