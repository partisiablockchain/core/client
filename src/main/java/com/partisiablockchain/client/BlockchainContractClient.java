package com.partisiablockchain.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.dto.AccountState;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.dto.ContractState;

/** A client for interacting with contracts and accounts on a Partisia blockchain (PBC). */
public interface BlockchainContractClient extends BlockchainClientForTransaction {

  @Override
  default long getNonce(BlockchainAddress blockchainAddress) {
    return getAccountState(blockchainAddress).nonce();
  }

  @Override
  default long getLatestProductionTime(ShardId shardid) {
    return getLatestBlock(shardid).productionTime();
  }

  /**
   * Retrieve an account state from the blockchain.
   *
   * @param address The address of the account.
   * @return The retrieved account state.
   */
  AccountState getAccountState(BlockchainAddress address);

  /**
   * Retrieve a contract state from the blockchain.
   *
   * @param address The address of the contract.
   * @return The retrieved contract state.
   */
  ContractState getContractState(BlockchainAddress address);

  /**
   * Retrieve the latest block from the targeted PBC.
   *
   * @param shardId The name of the shard to interact with.
   * @return The latest block.
   */
  BlockState getLatestBlock(ShardId shardId);
}
