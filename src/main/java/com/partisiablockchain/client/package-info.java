/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 *
 *
 * <pre>
 * Contains the client framework for interacting with the Partisia Blockchain.
 *
 * To send a transaction and wait for its complete execution on the chain, see the following
 * example that contacts the Partisia Testnet and mints gas to the recipient:
 *
 * {@code
 * BlockchainClient blockchain = BlockchainClient.create("https://node1.testnet.partisiablockchain.com", 3);
 * final String privateKey = "012345678901234567890123456789"; // use a real private key
 * final SenderAuthentication sender = SenderAuthenticationKeyPair.fromString(privateKey);
 * BlockchainTransactionClient txClient = BlockchainTransactionClient.create(blockchain, sender);
 * final BlockchainAddress byocFaucetAddress =
 *          BlockchainAddress.fromString("01860476afca938871ff2c49bf5490235445942e3e");
 * final String recipient = "00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed";
 * final byte[] rpc = SafeDataOutputStream.serialize(s -> s.write(Hex.decode(recipient)));
 * final Transaction transaction = Transaction.create(byocFaucetAddress, rpc);
 * final SentTransaction sentTransaction = txClient.signAndSend(transaction, 5910);
 * // Optional: Waiting for event execution guarantees
 * //           that the code from the transaction was run by the chain before returning
 * //           and may provide information about success or failure of execution.
 * final ExecutedTransactionTree transactionResult = txClient.waitForSpawnedEvents(sentTransaction);
 * }
 * </pre>
 *
 * @since 1.0
 * @version 1.0
 */
package com.partisiablockchain.client;
