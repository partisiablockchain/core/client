package com.partisiablockchain.client.shards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.util.NumericConversion;
import java.util.ArrayList;
import java.util.List;

/**
 * A shard manager that re-implements the routing plugin's distribution off addresses to find their
 * hosting shard. Keeps track of the shards of the blockchain.
 */
public final class ShardManager {

  private final List<ShardId> shards;

  /**
   * Creates a new shard manager for a specific number of shards.
   *
   * @param numberOfShards The number of non-governance shards on the targeted blockchain. 0 if the
   *     targeted PBC only has the governance-shard.
   */
  public ShardManager(int numberOfShards) {
    this.shards = createShardList(numberOfShards);
  }

  /**
   * Get the governance shard, which is the only shard guaranteed to be on any PBC.
   *
   * @return The governance shard having name 'null' by convention.
   */
  public ShardId getGovernanceShard() {
    return shards.get(shards.size() - 1);
  }

  /**
   * Gets the list of all shards on the managed PBC, including the governance shard.
   *
   * @return The list of all shards on the managed PBC.
   */
  public List<ShardId> getShards() {
    return shards;
  }

  /**
   * Finds the ID of the shard hosting the provided address.
   *
   * @param address The address whose shard is desired.
   * @return The name of the shard hosting the provided address.
   */
  public ShardId getShard(BlockchainAddress address) {
    return shards.get(findShardIndex(address));
  }

  private List<ShardId> createShardList(int numberOfShards) {
    if (numberOfShards < 0) {
      throw new IllegalArgumentException("Number of shards must not be negative!");
    }

    List<ShardId> shards = new ArrayList<>();
    for (int shardNumber = 0; shardNumber < numberOfShards; shardNumber++) {
      shards.add(new ShardId("Shard" + shardNumber));
    }

    // Governance shard. Should be last in list for indexing purposes.
    shards.add(new ShardId(null));

    return shards;
  }

  /**
   * Finds the index of the shard hosting the provided address.
   *
   * @param address The address of the contract to interact with.
   * @return The number of the shard that holds the input address.
   */
  int findShardIndex(BlockchainAddress address) {
    /* Start from the 16th byte of the identifier, so that we can convert to an int.
    This will still respect the entropy. */
    int lastFourBytes = NumericConversion.intFromBytes(address.getIdentifier(), 16);

    /*
     We exempt the governance shard unless it's the only one.
     This is done to match the way we allocate contracts and user addresses on the chain.
    */
    int dividend = shards.size() == 1 ? 1 : shards.size() - 1;
    return Math.abs(lastFourBytes) % dividend;
  }
}
