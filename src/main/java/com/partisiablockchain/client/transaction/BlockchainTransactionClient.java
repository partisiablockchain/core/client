package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClientForTransaction;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.Event;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.dto.TransactionPointer;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A client that supports signing, sending and waiting for transactions on the blockchain. */
public final class BlockchainTransactionClient {

  private static final Logger logger = LoggerFactory.getLogger(BlockchainTransactionClient.class);

  /**
   * How long a signed transaction is valid for inclusion in a block. This is three minutes in
   * milliseconds.
   */
  public static final long DEFAULT_TRANSACTION_VALIDITY_DURATION = 180_000L;

  /**
   * Default time out for a spawned event to be included in a block before timing out is ten minutes
   * in milliseconds.
   */
  public static final long DEFAULT_SPAWNED_EVENT_TIMEOUT = 600_000L;

  /** Client used for communicating with the blockchain. */
  private final BlockchainClientForTransaction blockchainClient;

  /** Authentication of the sender that will sign transactions. */
  private final SenderAuthentication authentication;

  /** Support for working with time (now and sleeping). */
  private final CurrentTime timeSupport;

  /**
   * The amount of time, in milliseconds, the client will wait for a spawned event to be included in
   * a block before timing out.
   */
  private final long spawnedEventTimeout;

  /**
   * The amount of time, in milliseconds, a signed transaction is valid for inclusion in a block.
   */
  private final long transactionValidityDuration;

  private final ConditionWaiter waiter;

  private BlockchainTransactionClient(
      BlockchainClientForTransaction blockchainClient,
      SenderAuthentication authentication,
      CurrentTime timeSupport,
      ConditionWaiter waiter,
      long transactionValidityDuration,
      long spawnedEventTimeout) {
    this.blockchainClient = Objects.requireNonNull(blockchainClient);
    this.authentication = Objects.requireNonNull(authentication);
    this.timeSupport = Objects.requireNonNull(timeSupport);
    this.waiter = Objects.requireNonNull(waiter);
    this.transactionValidityDuration = transactionValidityDuration;
    this.spawnedEventTimeout = spawnedEventTimeout;
  }

  /**
   * Create blockchain transaction client.
   *
   * @param blockchainClient The blockchain client used for communicating with the blockchain. Not
   *     nullable.
   * @param authentication Authentication of the sender that will sign transactions. Not nullable.
   * @return A blockchain transaction client
   */
  public static BlockchainTransactionClient create(
      BlockchainClientForTransaction blockchainClient, SenderAuthentication authentication) {
    return create(
        blockchainClient,
        authentication,
        DEFAULT_TRANSACTION_VALIDITY_DURATION,
        DEFAULT_SPAWNED_EVENT_TIMEOUT);
  }

  /**
   * Create blockchain transaction client.
   *
   * @param blockchainClient The blockchain client used for communicating with the blockchain. Not
   *     nullable.
   * @param authentication Authentication of the sender that will sign transactions. Not nullable.
   * @param transactionValidityDuration The amount of time, in milliseconds, a signed transaction is
   *     valid for inclusion in a block.
   * @param spawnedEventTimeout The amount of time, in milliseconds, the client will wait for a
   *     spawned event to be included in a block before timing out.
   * @return A blockchain transaction client
   */
  public static BlockchainTransactionClient create(
      BlockchainClientForTransaction blockchainClient,
      SenderAuthentication authentication,
      long transactionValidityDuration,
      long spawnedEventTimeout) {
    return new BlockchainTransactionClient(
        blockchainClient,
        authentication,
        CurrentTime.SYSTEM,
        ConditionWaiterImpl.create(),
        transactionValidityDuration,
        spawnedEventTimeout);
  }

  static BlockchainTransactionClient createForTest(
      BlockchainClientForTransaction blockchainClient,
      SenderAuthentication authentication,
      CurrentTime timeSupport,
      ConditionWaiter conditionWaiter,
      long transactionValidityDuration,
      long spawnedEventTimeout) {
    return new BlockchainTransactionClient(
        blockchainClient,
        authentication,
        timeSupport,
        conditionWaiter,
        transactionValidityDuration,
        spawnedEventTimeout);
  }

  /**
   * Sign a transaction in preparation for sending it to the blockchain. The signed transaction has
   * limited validity, since it includes information about the valid-to-time and the next available
   * nonce of the sending user.
   *
   * @param transaction The transaction to sign
   * @param gasCost The amount of gas to allocate for executing the transaction.
   * @return A signed transaction corresponding to the passed transaction.
   */
  public SignedTransaction sign(Transaction transaction, long gasCost) {
    return sign(transaction, gasCost, timeSupport.now() + transactionValidityDuration);
  }

  private SignedTransaction sign(Transaction transaction, long gasCost, long applicableUntil) {
    BlockchainAddress sender = authentication.getAddress();
    long nonce = blockchainClient.getNonce(sender);
    String chainId = blockchainClient.getChainId().chainId();
    long validToTime = timeSupport.now() + transactionValidityDuration;
    long effectiveValidToTime = Math.min(validToTime, applicableUntil);
    return SignedTransaction.create(
        authentication, nonce, effectiveValidToTime, gasCost, chainId, transaction);
  }

  /**
   * Sends a signed transaction to the blockchain for execution and inclusion in a block.
   *
   * @param signedTransaction The signed transaction to send.
   * @return A sent transaction corresponding to the signed transaction.
   */
  public SentTransaction send(SignedTransaction signedTransaction) {
    TransactionPointer transactionPointer = blockchainClient.putTransaction(signedTransaction);
    return new SentTransaction(signedTransaction, transactionPointer);
  }

  /**
   * Sign and send a transaction to the blockchain for execution.
   *
   * @param transaction The transaction to sign and send
   * @param gasCost The amount of gas to allocate for executing the transaction.
   * @return A sent transaction corresponding to the signed transaction.
   */
  public SentTransaction signAndSend(Transaction transaction, long gasCost) {
    return send(sign(transaction, gasCost, timeSupport.now() + transactionValidityDuration));
  }

  /**
   * Sign and sends a transaction to the blockchain for execution.
   *
   * @param transaction the transaction to sign and send
   * @param gasCost the amount of gas to allocate for executing the transaction
   * @return a sent transaction corresponding to the signed transaction, or null if an exception was
   *     caught.
   */
  private SentTransaction signAndSend(Transaction transaction, long gasCost, long applicableUntil) {
    try {
      return send(sign(transaction, gasCost, applicableUntil));
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Waits for a sent transaction to be included in a block on the blockchain.
   *
   * @param sentTransaction A transaction that has been sent to the blockchain.
   * @return The executed state of the transaction.
   */
  public ExecutedTransaction waitForInclusionInBlock(SentTransaction sentTransaction) {
    long validToTime = sentTransaction.signedTransaction().getValidToTime();
    long timeToWait = validToTime - timeSupport.now();
    ShardId senderShard = new ShardId(sentTransaction.transactionPointer().destinationShardId());
    Hash transactionIdentifier = sentTransaction.transactionPointer().identifier();
    return waitForTransaction(senderShard, transactionIdentifier, timeToWait);
  }

  /**
   * Waits for all recursive sub-events spawned by an executed transaction to be included in blocks
   * on the blockchain. If each sub-events is not included in a block within 10 minutes an exception
   * is thrown.
   *
   * @param executedTransaction An executed transaction.
   * @return The execution status for all recursive sub-events spawned by the transaction.
   */
  public ExecutedTransactionTree waitForSpawnedEvents(ExecutedTransaction executedTransaction) {
    List<ExecutedTransaction> executedEvents = new ArrayList<>();
    ArrayDeque<Event> pendingEvents = new ArrayDeque<>(executedTransaction.events());
    while (!pendingEvents.isEmpty()) {
      Event nextEvent = pendingEvents.removeFirst();
      Hash identifier = Hash.fromString(nextEvent.identifier());
      final ShardId shardId = new ShardId(nextEvent.destinationShard());
      ExecutedTransaction executed = waitForTransaction(shardId, identifier, spawnedEventTimeout);
      logger.debug("Execution of {} spawned event {}", nextEvent, executed.events().size());
      executedEvents.add(executed);
      pendingEvents.addAll(executed.events());
    }
    return new ExecutedTransactionTree(executedTransaction, executedEvents);
  }

  /**
   * Waits for a sent transaction and all recursive sub-events it spawns to be included in blocks on
   * the blockchain.
   *
   * @param sentTransaction A transaction that has been sent to the blockchain.
   * @return The executed state of the transaction and all recursive sub-events spawned by the
   *     transaction. If each sub-events is not included in a block within 10 minutes an exception
   *     is thrown.
   */
  public ExecutedTransactionTree waitForSpawnedEvents(SentTransaction sentTransaction) {
    final ExecutedTransaction executedTransaction = waitForInclusionInBlock(sentTransaction);
    return waitForSpawnedEvents(executedTransaction);
  }

  /**
   * Try to ensure that the given transaction is included in a final block before the specified time
   * has passed. Will try to resend the transaction until it has been included in a block or the
   * specified time has passed. If the transaction has not been included, or we are unable to
   * determine inclusion, due to network issues, an exception is thrown.
   *
   * @param transaction The transaction to sign and send
   * @param transactionGasCost The amount of gas to allocate for executing the transaction
   * @param includeBefore The UTC timestamp after which the transaction will no longer tried to be
   *     sent
   * @return The executed state of the transaction
   */
  public ExecutedTransaction ensureInclusionInBlock(
      Transaction transaction, long transactionGasCost, Instant includeBefore) {
    return ensureInclusionInBlock(
        transaction, transactionGasCost, includeBefore, Duration.ofMinutes(30));
  }

  /**
   * Try to ensure that the given transaction is included in a final block before the specified time
   * has passed. Will try to resend the transaction until it has been included in a block or the
   * specified time has passed. If the transaction has not been included, or we are unable to
   * determine inclusion, due to network issues, an exception is thrown.
   *
   * <p>To determine that a tried transaction has not been included in a block, we must wait until
   * the last valid block that could have included the transaction has been finalized. The caller of
   * this function must specify the amount of time to wait for the last valid block after validity
   * time of the tried transaction has expired.
   *
   * @param transaction The transaction to sign and send
   * @param transactionGasCost The amount of gas to allocate for executing the transaction
   * @param includeBefore The UTC timestamp after which the transaction will no longer tried to be
   *     sent
   * @param lastValidBlockWaitTime The amount of time to wait for the last valid block that a tried
   *     transaction can be included in
   * @return The executed state of the transaction
   */
  public ExecutedTransaction ensureInclusionInBlock(
      Transaction transaction,
      long transactionGasCost,
      Instant includeBefore,
      Duration lastValidBlockWaitTime) {
    long applicableUntil = includeBefore.toEpochMilli();
    while (applicableUntil > timeSupport.now()) {
      SentTransaction sentTransaction =
          waiter.waitForCondition(
              () -> signAndSend(transaction, transactionGasCost, applicableUntil),
              Objects::nonNull,
              applicableUntil - timeSupport.now(),
              "Unable to push transaction to chain");
      ExecutedTransaction executedTransaction =
          waitForExecutedTransaction(sentTransaction, lastValidBlockWaitTime);
      if (executedTransaction != null) {
        return executedTransaction;
      }
    }
    throw new RuntimeException("Transaction was not included in block before " + includeBefore);
  }

  private ExecutedTransaction waitForExecutedTransaction(
      SentTransaction sentTransaction, Duration latestBlockWait) {
    try {
      return waitForInclusionInBlock(sentTransaction);
    } catch (Exception ignore) {
      return waitForLastPossibleBlockToInclude(sentTransaction, latestBlockWait);
    }
  }

  private ExecutedTransaction waitForLastPossibleBlockToInclude(
      SentTransaction sentTransaction, Duration latestBlockWait) {
    ShardId shardId = new ShardId(sentTransaction.transactionPointer().destinationShardId());
    long validToTime = sentTransaction.signedTransaction().getValidToTime();
    long timeout = latestBlockWait.toMillis() + validToTime;
    waiter.waitForCondition(
        () -> blockchainClient.getLatestProductionTime(shardId),
        (productionTime) -> productionTime > validToTime,
        timeout,
        () ->
            String.format(
                "Unable to get block with production time greater than %d for shard %s",
                validToTime, shardId));
    Hash transactionIdentifier = sentTransaction.transactionPointer().identifier();
    try {
      return blockchainClient.getTransaction(shardId, transactionIdentifier);
    } catch (Exception ignore) {
      return null;
    }
  }

  /**
   * Waits for the execution of a single transaction or sub-transaction (an event).
   *
   * @param shardId The shard to wait at.
   * @param transactionIdentifier The identifier of the transaction to wait for.
   * @param timeout How long to wait for the transaction before throwing an exception (in
   *     milliseconds).
   * @return The transaction being waited upon, post-execution.
   */
  private ExecutedTransaction waitForTransaction(
      ShardId shardId, Hash transactionIdentifier, long timeout) {
    return waiter.waitForCondition(
        () -> blockchainClient.getTransaction(shardId, transactionIdentifier),
        Objects::nonNull,
        timeout,
        () ->
            String.format(
                "Transaction '%s' was not included in a block at shard '%s'",
                transactionIdentifier, shardId.name()));
  }

  /** Extension interface used internally in testing. */
  @FunctionalInterface
  interface CurrentTime {

    /** Default implementation is the system clock. */
    CurrentTime SYSTEM = System::currentTimeMillis;

    /**
     * Current time from epoch.
     *
     * @return current time in milliseconds
     */
    long now();
  }
}
