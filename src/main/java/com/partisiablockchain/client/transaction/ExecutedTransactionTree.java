package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.dto.ExecutedTransaction;
import java.util.List;
import java.util.Objects;

/**
 * Execution status for a transaction and all the sub-events it spawned on the blockchain.
 *
 * <p>The transaction and events form a tree that can be explored through {@link
 * ExecutedTransaction#events()}.
 */
public final class ExecutedTransactionTree {

  /** Execution status for the initiating transaction. */
  private final ExecutedTransaction executedTransaction;

  /** Execution status for all sub-events spawned by the initiating transaction. */
  private final List<ExecutedTransaction> executedEvents;

  /**
   * Create execution status for a transaction and any spawned events.
   *
   * @param executedTransaction Execution status for a transaction that was included in a block. Not
   *     nullable.
   * @param executedEvents Execution status for all sub-events spawned by the transaction. Not
   *     nullable.
   */
  ExecutedTransactionTree(
      ExecutedTransaction executedTransaction, List<ExecutedTransaction> executedEvents) {
    this.executedTransaction = Objects.requireNonNull(executedTransaction);
    this.executedEvents = List.copyOf(executedEvents);
  }

  /**
   * Get execution status for the initiating transaction.
   *
   * @return The execution status. Never null.
   */
  public ExecutedTransaction executedTransaction() {
    return executedTransaction;
  }

  /**
   * Get execution status for all sub-events spawned by the initiating transaction.
   *
   * @return Execution status for resulting sub-events. Never null.
   */
  public List<ExecutedTransaction> executedEvents() {
    return executedEvents;
  }

  /**
   * Check if the executed transaction tree includes any transactions or events which did not
   * succeed.
   *
   * @return true if the transaction tree includes any failures, false if all succeeded.
   */
  public boolean hasFailures() {
    if (!executedTransaction().executionSucceeded()) {
      return true;
    }
    for (ExecutedTransaction executedEvent : executedEvents()) {
      if (!executedEvent.executionSucceeded()) {
        return true;
      }
    }
    return false;
  }
}
