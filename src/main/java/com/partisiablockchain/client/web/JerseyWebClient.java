package com.partisiablockchain.client.web;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/** A web client based on jersey. */
public final class JerseyWebClient implements WebClient {

  private final Client client;

  /**
   * Create a new jersey web client.
   *
   * @param client the actual client
   */
  public JerseyWebClient(Client client) {
    this.client = Objects.requireNonNull(client);
  }

  @Override
  public <T> T get(
      String url, Map<String, Object> queryParams, Class<T> clazz, Map<String, Object> templates) {
    return buildGetRequest(url, queryParams, templates).get(clazz);
  }

  @Override
  public <T> T get(
      String url,
      Map<String, Object> queryParams,
      GenericType<T> type,
      Map<String, Object> templates) {
    return buildGetRequest(url, queryParams, templates).get(type);
  }

  private Invocation.Builder buildGetRequest(
      String url, Map<String, Object> queryParams, Map<String, Object> templates) {
    WebTarget webTarget = client.target(url);
    List<Map.Entry<String, Object>> entries =
        queryParams.entrySet().stream().sorted(Map.Entry.comparingByKey()).toList();
    for (Map.Entry<String, Object> entry : entries) {
      webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
    }
    return webTarget.resolveTemplates(templates).request();
  }

  @Override
  public void put(String url, Object entity) {
    Response put =
        client.target(url).request().put(Entity.entity(entity, MediaType.APPLICATION_JSON_TYPE));
    if (!put.getStatusInfo().getFamily().equals(Response.Status.Family.SUCCESSFUL)) {
      throw new RuntimeException(put.getStatus() + ": " + put.getStatusInfo());
    }
  }

  @Override
  public <T> T put(String url, Object entity, Class<T> type) {
    return client
        .target(url)
        .request()
        .put(Entity.entity(entity, MediaType.APPLICATION_JSON_TYPE), type);
  }

  @Override
  public <RequestT, ResponseT> ResponseT post(
      String url, RequestT payload, Class<ResponseT> responseClazz, Map<String, Object> templates) {
    return client
        .target(url)
        .resolveTemplates(templates)
        .request()
        .post(Entity.entity(payload, MediaType.APPLICATION_JSON_TYPE), responseClazz);
  }
}
