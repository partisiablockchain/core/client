package com.partisiablockchain.client.web;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import jakarta.ws.rs.core.GenericType;
import java.util.Map;

/** Generic web client for creating http request and getting a typed answer back. */
public interface WebClient {

  /**
   * GET request.
   *
   * @param url the url to get
   * @param clazz the return type
   * @param <T> the type of the return
   * @return the GET result.
   */
  default <T> T get(String url, Class<T> clazz) {
    return get(url, clazz, Map.of());
  }

  /**
   * GET request.
   *
   * @param url the url to get
   * @param clazz the return type
   * @param templates templates to replace in the url
   * @param <T> the type of the return
   * @return the GET result.
   */
  default <T> T get(String url, Class<T> clazz, Map<String, Object> templates) {
    return get(url, Map.of(), clazz, templates);
  }

  /**
   * GET request.
   *
   * @param url the url to get
   * @param queryParams the query params to use for get
   * @param clazz the return type
   * @param templates templates to replace in the url
   * @param <T> the type of the return
   * @return the GET result.
   */
  <T> T get(
      String url, Map<String, Object> queryParams, Class<T> clazz, Map<String, Object> templates);

  /**
   * GET request.
   *
   * @param url the url to get
   * @param type the return type
   * @param <T> the type of the return
   * @return the GET result.
   */
  default <T> T get(String url, GenericType<T> type) {
    return get(url, type, Map.of());
  }

  /**
   * GET request.
   *
   * @param url the url to get
   * @param type the return type
   * @param templates templates to replace in the url
   * @param <T> the type of the return
   * @return the GET result.
   */
  default <T> T get(String url, GenericType<T> type, Map<String, Object> templates) {
    return get(url, Map.of(), type, templates);
  }

  /**
   * GET request.
   *
   * @param url the url to get
   * @param queryParams the query params to use for get
   * @param type the return type
   * @param templates templates to replace in the url
   * @param <T> the type of the return
   * @return the GET result.
   */
  <T> T get(
      String url,
      Map<String, Object> queryParams,
      GenericType<T> type,
      Map<String, Object> templates);

  /**
   * PUT request.
   *
   * @param url the url to put to
   * @param entity the entity to put
   */
  void put(String url, Object entity);

  /**
   * PUT request with return class value.
   *
   * @param url url the url to put to
   * @param entity entity the entity to put
   * @param clazz the return type
   * @param <T> the type of the return
   * @return the PUT result.
   */
  <T> T put(String url, Object entity, Class<T> clazz);

  /**
   * POST request.
   *
   * @param url the url to post to
   * @param payload the entity to put
   * @param responseClazz the return type
   * @param <RequestT> the type of the return
   * @param <ResponseT> the type of the request entity
   * @return the POST result.
   */
  default <RequestT, ResponseT> ResponseT post(
      String url, RequestT payload, Class<ResponseT> responseClazz) {
    return post(url, payload, responseClazz, Map.of());
  }

  /**
   * POST request.
   *
   * @param url the url to post to
   * @param payload the entity to put
   * @param responseClazz the return type
   * @param templates templates to replace in the url
   * @param <RequestT> the type of the return
   * @param <ResponseT> the type of the request entity
   * @return the POST result.
   */
  <RequestT, ResponseT> ResponseT post(
      String url, RequestT payload, Class<ResponseT> responseClazz, Map<String, Object> templates);
}
