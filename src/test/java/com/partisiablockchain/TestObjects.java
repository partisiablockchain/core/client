package com.partisiablockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.KeyPair;
import java.math.BigInteger;

/** Test. */
public final class TestObjects {

  public static final KeyPair KEY_ONE = new KeyPair(BigInteger.ONE);

  public static final String PRIVATE_KEY = "abc";

  public static final BlockchainAddress ACCOUNT_ONE =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");

  public static final BlockchainAddress ACCOUNT_TWO =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");

  public static final BlockchainAddress CONTRACT_ONE =
      BlockchainAddress.fromString("01A000000000000000000000000000000000000001");
  public static final BlockchainAddress CONTRACT_TWO =
      BlockchainAddress.fromString("01A000000000000000000000000000000000000002");
}
