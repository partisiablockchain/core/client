package com.partisiablockchain.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.mockito.Mockito.when;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.Response;
import java.util.Map;
import org.glassfish.jersey.client.JerseyInvocation;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

/** Test. */
public abstract class AbstractJerseyTest {

  @SuppressWarnings("rawtypes")
  protected ArgumentCaptor<Class> lastGet;

  @SuppressWarnings("rawtypes")
  protected ArgumentCaptor<GenericType> lastGenericGet;

  @SuppressWarnings("rawtypes")
  protected ArgumentCaptor<Entity> lastPut;

  @SuppressWarnings("rawtypes")
  protected ArgumentCaptor<Entity> lastPost;

  @SuppressWarnings("rawtypes")
  protected ArgumentCaptor<Class> lastPutClass;

  protected ArgumentCaptor<String> urlCaptor;
  protected ArgumentCaptor<Map<String, Object>> mapCaptor;
  protected ArgumentCaptor<String> queryParamKeyCaptor;
  protected ArgumentCaptor<Object> queryParamValueCaptor;
  protected Client mocked;
  protected Object expectedResult;
  protected Response putResult;

  /** Set up the test. */
  @SuppressWarnings("unchecked")
  @BeforeEach
  public void setUp() {
    lastGet = ArgumentCaptor.forClass(Class.class);
    lastGenericGet = ArgumentCaptor.forClass(GenericType.class);
    lastPut = ArgumentCaptor.forClass(Entity.class);
    lastPutClass = ArgumentCaptor.forClass(Class.class);
    lastPost = ArgumentCaptor.forClass(Entity.class);
    urlCaptor = ArgumentCaptor.forClass(String.class);
    mapCaptor = ArgumentCaptor.forClass(Map.class);
    queryParamKeyCaptor = ArgumentCaptor.forClass(String.class);
    queryParamValueCaptor = ArgumentCaptor.forClass(Object.class);
    putResult = Response.noContent().build();

    mocked = Mockito.mock(Client.class);
    WebTarget webTarget = createWebTarget();

    when(mocked.target(urlCaptor.capture())).thenAnswer(ignored -> webTarget);
    when(webTarget.queryParam(queryParamKeyCaptor.capture(), queryParamValueCaptor.capture()))
        .then(ignored -> webTarget);

    when(webTarget.resolveTemplates(mapCaptor.capture())).then(ignored -> createWebTarget());
  }

  private WebTarget createWebTarget() {
    WebTarget mock = Mockito.mock(WebTarget.class);
    when(mock.request()).then(ignored -> createRequest());
    return mock;
  }

  /**
   * Create a mocked request based on the configuration of the test.
   *
   * @return the mocked request.
   */
  @SuppressWarnings("unchecked")
  protected JerseyInvocation.Builder createRequest() {
    JerseyInvocation.Builder mock = Mockito.mock(JerseyInvocation.Builder.class);
    when(mock.get(lastGet.capture())).thenReturn(expectedResult);
    when(mock.get(lastGenericGet.capture())).thenReturn(expectedResult);
    when(mock.put(lastPut.capture())).thenReturn(putResult);
    when(mock.put(lastPut.capture(), lastPutClass.capture())).thenReturn(expectedResult);
    when(mock.post(lastPost.capture())).thenReturn(null);
    when(mock.post(lastPost.capture(), Mockito.any(Class.class))).thenReturn(expectedResult);
    return mock;
  }
}
