package com.partisiablockchain.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.partisiablockchain.client.shards.ShardId;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.ValueSource;

/** Testing of {@link BlockchainClient}. */
public final class BlockchainClientBaseUrlTest {

  /** Test that malformed urls fails. */
  @ParameterizedTest
  @EmptySource
  @ValueSource(strings = {"bad whitespace", "http://", "http://local host:8000"})
  public void malformedBaseUrl(String baseUrl) {
    assertThatCode(() -> BlockchainClient.create(baseUrl, 0))
        .as(baseUrl)
        .isInstanceOf(IllegalArgumentException.class);
  }

  /** Test that urls that are not malformed, but not supported by the client fails. */
  @ParameterizedTest
  @EmptySource
  @ValueSource(
      strings = {
        "no-protocol",
        "ftp://bad-protocol",
        "http://no.fragments#allowed",
        "http://no.query?allowed",
        "mailto:pbc@www.example.com",
        "news:comp.lang.pbc",
        "urn:isbn:096139210x",
        "sample/a/index.html#28",
        "../../demo/b/index.html",
        "file:/test/my/path",
        "file://test/my/path",
        "file:///test/my/path",
        "file:///test/my/path/",
        "file:///~/calendar",
        "/////////////////////////"
      })
  public void invalidBaseUrl(String baseUrl) {
    assertThatCode(() -> BlockchainClient.create(baseUrl, 0))
        .as(baseUrl)
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageStartingWith("Base url '" + baseUrl + "' was not of the expected format:");
  }

  /** Test that a {@link BlockchainClient} can be created with a valid URL. */
  @ParameterizedTest
  @ValueSource(
      strings = {
        "http://org",
        "http://example.org",
        "https://example.org",
        "http://localhost:8000",
        "http://Crane-BP1",
        "http://trailing.slashes/allowed/",
        "http://not.just.a/hostname",
        "http://localhost:9000/some/path",
        "http://127.0.0.1:9000/some/path",
        "http://example.org//some////weird///////amount//of/slashes////////",
        "http://FT-NODE-1"
      })
  public void validBaseUrl(String baseUrl) {
    final BlockchainClient blockchainClient = BlockchainClient.create(baseUrl, 0);
    assertThat(blockchainClient).isNotNull();
  }

  /** Test that a single trailing slash is stripped from the url path. */
  @ParameterizedTest
  @ValueSource(
      strings = {
        "https://example.org",
        "https://example.org/",
      })
  public void testFinalizeRestEndpoint(String baseUrl) {
    final BlockchainClient blockchainClient = BlockchainClient.create(baseUrl, 0);
    assertThat(blockchainClient.finalizeRestEndpoint(new ShardId(null), "/some/path"))
        .isEqualTo("https://example.org/blockchain/some/path");

    assertThat(blockchainClient.finalizeRestEndpoint(new ShardId("Shard1"), "/some/path"))
        .isEqualTo("https://example.org/shards/Shard1/blockchain/some/path");
  }
}
