package com.partisiablockchain.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.TextNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.client.web.JerseyWebClient;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.crypto.SignatureProvider;
import com.partisiablockchain.dto.AccountState;
import com.partisiablockchain.dto.AvlStateValue;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.dto.Event;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.dto.FailureCause;
import com.partisiablockchain.dto.Feature;
import com.partisiablockchain.dto.IncomingTransaction;
import com.partisiablockchain.dto.JarState;
import com.partisiablockchain.dto.traversal.TraversePath;
import com.secata.stream.SafeDataInputStream;
import jakarta.ws.rs.core.GenericType;
import java.math.BigInteger;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.LongSupplier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Testing of {@link BlockchainClient}. */
public final class BlockchainClientTest extends AbstractJerseyTest {

  private static final String baseUrl = "http://localhost";
  private static final Hash hash =
      Hash.fromString("a000000000000000000000000000000000000000000000000000000000000000");

  private BlockchainClient blockchainClient;
  private static final long VALIDITY_PERIOD = 100L;
  private static final long COST = 100L;

  /**
   * Creates a new blockchain client with a supplied web-client.
   *
   * @param baseUrl The base of the url of the targeted PBC.
   * @param numberOfShards The number of shards at the targeted PBC.
   * @param webClient A web client that communicates via HTTP.
   * @return the created client
   */
  public static BlockchainClient create(String baseUrl, int numberOfShards, WebClient webClient) {
    return new BlockchainClient(baseUrl, numberOfShards, webClient);
  }

  /** Set up the test. */
  @BeforeEach
  @Override
  public void setUp() {
    super.setUp();

    JerseyWebClient webClient = new JerseyWebClient(mocked);
    blockchainClient = new BlockchainClient(baseUrl, 2, webClient);
  }

  @Test
  public void latestBlock() {
    assertThat(blockchainClient.getLatestBlock(new ShardId("Shard1"))).isNull();
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/shards/Shard1/blockchain/blocks/latest");
    assertThat(lastGet.getValue()).isEqualTo(BlockState.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void latestNonNullBlock() {
    expectedResult = new BlockState("", 0, 0, 0, null, null, "", "", (short) 0);
    BlockState latestBlock = blockchainClient.getLatestBlock(new ShardId(null));
    assertThat(latestBlock).isEqualTo(expectedResult);
  }

  @Test
  public void getLatestBlockBeforeTime() {
    assertThat(blockchainClient.getLatestBlockBeforeTime(new ShardId(null), 1000)).isNull();
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/blockchain/blocks/latestBlockTime");
    assertThat(queryParamKeyCaptor.getValue()).isEqualTo("utcTime");
    assertThat(queryParamValueCaptor.getValue()).isEqualTo(1000L);
    assertThat(lastGet.getValue()).isEqualTo(Long.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void latestNonNullBlockBeforeTime() {
    expectedResult = 123L;
    Long latestBlock = blockchainClient.getLatestBlockBeforeTime(new ShardId(null), 1000);
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/blockchain/blocks/latestBlockTime");
    assertThat(queryParamKeyCaptor.getValue()).isEqualTo("utcTime");
    assertThat(queryParamValueCaptor.getValue()).isEqualTo(1000L);
    assertThat(latestBlock).isEqualTo(123L);
  }

  @Test
  public void blockByHash() {
    expectedResult = new BlockState("", 0, 0, 0, List.of(), List.of(), "", "", (short) 0);
    assertThat(blockchainClient.getBlockByHash(new ShardId(null), hash))
        .isInstanceOf(BlockState.class);
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/blockchain/blocks/{blockHash}");
    assertThat(mapCaptor.getValue().get("blockHash")).isEqualTo(hash.toString());
    assertThat(lastGet.getValue()).isEqualTo(BlockState.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void blockByNumber() {
    expectedResult = new BlockState("", 0, 0, 0, List.of(), List.of(), "", "", (short) 0);
    assertThat(blockchainClient.getBlockByNumber(new ShardId(null), 21))
        .isInstanceOf(BlockState.class);
    assertThat(urlCaptor.getValue())
        .isEqualTo(baseUrl + "/blockchain/blocks/blockTime/{blockTime}");
    assertThat(mapCaptor.getValue().get("blockTime")).isEqualTo("21");
    assertThat(lastGet.getValue()).isEqualTo(BlockState.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void creation() {
    BlockchainClient client = BlockchainClient.create(baseUrl, 0);
    assertThat(client).isNotNull();
  }

  @Test
  public void shardManager() {
    assertThat(blockchainClient.getShardManager()).isNotNull();
    assertThat(blockchainClient.getShardManager().getShards().size()).isEqualTo(3);
  }

  @Test
  public void features() {
    expectedResult = new Feature("Name", "Value");
    assertThat(blockchainClient.getFeature("Feature"))
        .usingRecursiveComparison()
        .isEqualTo(expectedResult);
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/blockchain/features/{feature}");
    assertThat(mapCaptor.getValue().get("feature")).isEqualTo("Feature");
    assertThat(lastGet.getValue()).isEqualTo(Feature.class);
  }

  @Test
  public void contractState() {
    BlockchainAddress addressAtShard1 =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    byte[] data = new byte[] {};
    expectedResult = new ContractState(null, "", "", 0, null, data);

    assertThat(blockchainClient.getContractState(addressAtShard1))
        .isInstanceOf(ContractState.class);
    assertThat(urlCaptor.getValue())
        .isEqualTo(baseUrl + "/shards/Shard1/blockchain/contracts/{address}");
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");

    assertThat(lastGet.getValue()).isEqualTo(ContractState.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getContractStateWithNoneParameter() {
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    byte[] data = new byte[] {};
    expectedResult = new ContractState(null, "", "", 0, null, data);

    assertThat(blockchainClient.getContractState(address, BlockchainClient.StateOutput.NONE))
        .isInstanceOf(ContractState.class);
    assertThat(urlCaptor.getValue())
        .isEqualTo(baseUrl + "/shards/Shard1/blockchain/contracts/{address}");
    assertThat(queryParamKeyCaptor.getAllValues())
        .containsExactly("blockTime", BlockchainClient.StateOutput.STATE_OUTPUT);
    assertThat(queryParamValueCaptor.getAllValues())
        .containsExactly(
            -1L, BlockchainClient.StateOutput.NONE.name().toLowerCase(Locale.getDefault()));
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");

    assertThat(lastGet.getValue()).isEqualTo(ContractState.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getContractStateWithJsonParameter() {
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    byte[] data = new byte[] {};
    expectedResult = new ContractState(null, "", "", 0, null, data);

    assertThat(blockchainClient.getContractState(address, BlockchainClient.StateOutput.JSON))
        .isInstanceOf(ContractState.class);
    assertThat(urlCaptor.getValue())
        .isEqualTo(baseUrl + "/shards/Shard1/blockchain/contracts/{address}");
    assertThat(queryParamKeyCaptor.getAllValues())
        .containsExactly("blockTime", BlockchainClient.StateOutput.STATE_OUTPUT);
    assertThat(queryParamValueCaptor.getAllValues())
        .containsExactly(
            -1L, BlockchainClient.StateOutput.JSON.name().toLowerCase(Locale.getDefault()));
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");

    assertThat(lastGet.getValue()).isEqualTo(ContractState.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getContractStateWithBinaryParameter() {
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    byte[] data = new byte[] {};
    expectedResult = new ContractState(null, "", "", 0, null, data);

    assertThat(blockchainClient.getContractState(address, BlockchainClient.StateOutput.BINARY))
        .isInstanceOf(ContractState.class);
    assertThat(urlCaptor.getValue())
        .isEqualTo(baseUrl + "/shards/Shard1/blockchain/contracts/{address}");
    assertThat(queryParamKeyCaptor.getAllValues())
        .containsExactly("blockTime", BlockchainClient.StateOutput.STATE_OUTPUT);
    assertThat(queryParamValueCaptor.getAllValues())
        .containsExactly(
            -1L, BlockchainClient.StateOutput.BINARY.name().toLowerCase(Locale.getDefault()));
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");

    assertThat(lastGet.getValue()).isEqualTo(ContractState.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getContractStateWithAvlBinaryParameter() {
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    byte[] data = new byte[] {};
    expectedResult = new ContractState(null, "", "", 0, null, data);

    assertThat(blockchainClient.getContractState(address, BlockchainClient.StateOutput.AVL_BINARY))
        .isInstanceOf(ContractState.class);
    assertThat(urlCaptor.getValue())
        .isEqualTo(baseUrl + "/shards/Shard1/blockchain/contracts/{address}");
    assertThat(queryParamKeyCaptor.getAllValues())
        .containsExactly("blockTime", BlockchainClient.StateOutput.STATE_OUTPUT);
    assertThat(queryParamValueCaptor.getAllValues())
        .containsExactly(
            -1L, BlockchainClient.StateOutput.AVL_BINARY.name().toLowerCase(Locale.getDefault()));
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");

    assertThat(lastGet.getValue()).isEqualTo(ContractState.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getContractStateAvlNextN() {
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    Map.Entry<byte[], byte[]> entry = Map.entry(new byte[] {0}, new byte[] {1});
    expectedResult = List.of(entry);

    assertThat(blockchainClient.getContractStateAvlNextN(address, 0, new byte[1], 5, 1600000))
        .isInstanceOf(List.class)
        .containsExactly(entry);
    assertThat(urlCaptor.getValue())
        .isEqualTo(
            baseUrl + "/shards/Shard1/blockchain/contracts/{address}/avl/{treeId}/next/{key}");
    assertThat(queryParamKeyCaptor.getAllValues()).containsExactly("blockTime", "n");
    assertThat(queryParamValueCaptor.getAllValues()).containsExactly(1600000L, 5);
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");
    assertThat(mapCaptor.getValue().get("treeId")).isEqualTo(0);
    assertThat(mapCaptor.getValue().get("key")).isEqualTo("00");

    assertThat(lastGenericGet.getValue())
        .isEqualTo(new GenericType<List<Map.Entry<byte[], byte[]>>>() {});
    assertThat(lastGet.getAllValues()).isEmpty();
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getContractStateAvlFirstN() {
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    Map.Entry<byte[], byte[]> entry = Map.entry(new byte[] {0}, new byte[] {1});
    expectedResult = List.of(entry);

    assertThat(blockchainClient.getContractStateAvlNextN(address, 0, null, 5))
        .isInstanceOf(List.class)
        .containsExactly(entry);
    assertThat(urlCaptor.getValue())
        .isEqualTo(baseUrl + "/shards/Shard1/blockchain/contracts/{address}/avl/{treeId}/next");
    assertThat(queryParamKeyCaptor.getAllValues()).containsExactly("blockTime", "n");
    assertThat(queryParamValueCaptor.getAllValues()).containsExactly(-1L, 5);
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");
    assertThat(mapCaptor.getValue().get("treeId")).isEqualTo(0);

    assertThat(lastGenericGet.getValue())
        .isEqualTo(new GenericType<List<Map.Entry<byte[], byte[]>>>() {});
    assertThat(lastGet.getAllValues()).isEmpty();
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getContractStateAvlValue() {
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    expectedResult = new AvlStateValue(new byte[] {1});

    assertThat(blockchainClient.getContractStateAvlValue(address, 0, new byte[] {0}).data())
        .isInstanceOf(byte[].class)
        .containsExactly(1);
    assertThat(urlCaptor.getValue())
        .isEqualTo(baseUrl + "/shards/Shard1/blockchain/contracts/{address}/avl/{treeId}/{key}");
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");
    assertThat(mapCaptor.getValue().get("treeId")).isEqualTo(0);
    assertThat(mapCaptor.getValue().get("key")).isEqualTo("00");

    assertThat(lastGet.getValue()).isEqualTo(AvlStateValue.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getContractStateAvlDiffNext() {
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    byte[] key = new byte[] {0};
    expectedResult = List.of(key);

    assertThat(
            blockchainClient.getContractStateAvlModifiedKeys(address, 0, 100, 200, new byte[1], 5))
        .isInstanceOf(List.class)
        .containsExactly(key);
    assertThat(urlCaptor.getValue())
        .isEqualTo(
            baseUrl
                + "/shards/Shard1/blockchain/contracts/{address}"
                + "/avl/{treeId}/diff/{blockTime1}/{blockTime2}/{key}");
    assertThat(queryParamKeyCaptor.getValue()).isEqualTo("n");
    assertThat(queryParamValueCaptor.getValue()).isEqualTo(5);
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");
    assertThat(mapCaptor.getValue().get("treeId")).isEqualTo(0);
    assertThat(mapCaptor.getValue().get("key")).isEqualTo("00");
    assertThat(mapCaptor.getValue().get("blockTime1")).isEqualTo(100L);
    assertThat(mapCaptor.getValue().get("blockTime2")).isEqualTo(200L);

    assertThat(lastGenericGet.getValue()).isEqualTo(new GenericType<List<byte[]>>() {});
    assertThat(lastGet.getAllValues()).isEmpty();
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getContractStateAvlDiffFirst() {
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    byte[] key = new byte[] {0};
    expectedResult = List.of(key);

    assertThat(blockchainClient.getContractStateAvlModifiedKeys(address, 0, 100, 200, null, 5))
        .isInstanceOf(List.class)
        .containsExactly(key);
    assertThat(urlCaptor.getValue())
        .isEqualTo(
            baseUrl
                + "/shards/Shard1/blockchain/contracts/{address}"
                + "/avl/{treeId}/diff/{blockTime1}/{blockTime2}");
    assertThat(queryParamKeyCaptor.getValue()).isEqualTo("n");
    assertThat(queryParamValueCaptor.getValue()).isEqualTo(5);
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");
    assertThat(mapCaptor.getValue().get("treeId")).isEqualTo(0);
    assertThat(mapCaptor.getValue().get("blockTime1")).isEqualTo(100L);
    assertThat(mapCaptor.getValue().get("blockTime2")).isEqualTo(200L);

    assertThat(lastGenericGet.getValue()).isEqualTo(new GenericType<List<byte[]>>() {});
    assertThat(lastGet.getAllValues()).isEmpty();
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void traverseContractState() {
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");
    TraversePath path = new TraversePath(List.of());
    expectedResult = new TextNode("Result");
    assertThat(blockchainClient.traverseContractState(new ShardId(null), address, path))
        .isInstanceOf(TextNode.class);
    assertThat(urlCaptor.getValue()).isEqualTo("" + baseUrl + "/blockchain/contracts/{address}");
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");
    assertThat(lastPost.getValue().getEntity()).isEqualTo(path);
    assertThat(lastGet.getAllValues()).isEmpty();
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void accountState() {
    BlockchainAddress addressAtShard1 =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    assertThat(blockchainClient.getAccountState(addressAtShard1)).isNull();
    assertThat(urlCaptor.getValue())
        .isEqualTo(baseUrl + "/shards/Shard1/blockchain/account/{address}");
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");

    assertThat(lastGet.getValue()).isEqualTo(AccountState.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getAccountInfo() {
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");
    assertThat(blockchainClient.getAccountInfo(address)).isNull();
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/chain/accounts/{address}");
    assertThat(mapCaptor.getValue().get("address"))
        .isEqualTo("006810f243932574973f750f29e23637588084b95b");

    assertThat(lastGet.getValue()).isEqualTo(JsonNode.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getAccountInfoNonNull() {
    WebClientTest client = setupWebClient();
    blockchainClient = new BlockchainClient(baseUrl, 0, client);
    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");
    assertThat(blockchainClient.getAccountInfo(address).get("shardId").asText())
        .isEqualTo("Shard0");
  }

  @Test
  public void transaction() {
    Hash hash = Hash.fromString("0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a");

    byte[] data = new byte[] {};
    expectedResult =
        new ExecutedTransaction(
            data, "", 0, 0, "", "", true, List.of(), false, false, null, new FailureCause("", ""));
    assertThat(blockchainClient.getTransaction(new ShardId(null), hash))
        .isInstanceOf(ExecutedTransaction.class);
    assertThat(urlCaptor.getValue())
        .isEqualTo("" + baseUrl + "/blockchain/transaction/{transactionHash}");
    assertThat(mapCaptor.getValue().get("transactionHash"))
        .isEqualTo("0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a");

    assertThat(lastGet.getValue()).isEqualTo(ExecutedTransaction.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void transactionsByBlockHash() {
    Hash hash = Hash.fromString("0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a");
    byte[] data = new byte[] {};
    ExecutedTransaction executedTransaction =
        new ExecutedTransaction(
            data, "", 0, 0, "", "", true, List.of(), false, false, null, new FailureCause("", ""));

    expectedResult = List.of(executedTransaction);
    assertThat(blockchainClient.getTransactionsForBlock(new ShardId(null), hash))
        .isInstanceOf(List.class)
        .containsExactly(executedTransaction);
    assertThat(urlCaptor.getValue())
        .isEqualTo("" + baseUrl + "/blockchain/blocks/{blockHash}/transactions");
    assertThat(mapCaptor.getValue().get("blockHash")).isEqualTo(hash.toString());

    assertThat(lastGenericGet.getValue())
        .isEqualTo(new GenericType<List<ExecutedTransaction>>() {});
    assertThat(lastGet.getAllValues()).isEmpty();
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void globalAccountState() {
    TraversePath path = new TraversePath(List.of());
    expectedResult = new TextNode("Result");
    assertThat(blockchainClient.traverseGlobalAccountPlugin(path)).isInstanceOf(TextNode.class);
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/blockchain/accountPlugin/global");
    assertThat(lastPost.getValue().getEntity()).isEqualTo(path);
  }

  @Test
  public void localAccountState() {
    TraversePath path = new TraversePath(List.of());
    expectedResult = new TextNode("Result");
    assertThat(blockchainClient.traverseLocalAccountPlugin(new ShardId(null), path))
        .isInstanceOf(TextNode.class);
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/blockchain/accountPlugin/local");
    assertThat(lastPost.getValue().getEntity()).isEqualTo(path);
  }

  @Test
  public void globalConsensusState() {
    TraversePath path = new TraversePath(List.of());
    expectedResult = new TextNode("Result");
    assertThat(blockchainClient.traverseGlobalConsensusPlugin(path)).isInstanceOf(TextNode.class);
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/blockchain/consensusPlugin/global");
    assertThat(lastPost.getValue().getEntity()).isEqualTo(path);
  }

  @Test
  public void localConsensusState() {
    TraversePath path = new TraversePath(List.of());
    expectedResult = new TextNode("Result");
    assertThat(blockchainClient.traverseLocalConsensusPlugin(new ShardId(null), path))
        .isInstanceOf(TextNode.class);
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/blockchain/consensusPlugin/local");
    assertThat(lastPost.getValue().getEntity()).isEqualTo(path);
  }

  @Test
  public void consensusPluginJar() {
    byte[] data = new byte[] {9, 8, 7};
    JarState jar = new JarState(data);
    expectedResult = jar;
    assertThat(blockchainClient.getConsensusPluginJar()).isEqualTo(jar);
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/blockchain/consensusJar");
    assertThat(lastGet.getValue()).isEqualTo(JarState.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void event() {
    Event expectedEvent = new Event("id", "shard");
    expectedResult = expectedEvent;
    Hash identifier = Hash.create(stream -> stream.writeInt(9999));
    Event event = blockchainClient.getEvent(new ShardId(null), identifier);
    assertThat(event).isEqualTo(expectedEvent);
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/blockchain/event/{eventHash}");
    assertThat(mapCaptor.getValue().get("eventHash")).isEqualTo(identifier.toString());
  }

  @Test
  public void putEvent() {
    byte[] payload = new byte[22];
    IncomingTransaction event = new IncomingTransaction(payload);
    blockchainClient.putEvent(new ShardId(null), event);
    assertThat(urlCaptor.getValue()).isEqualTo(baseUrl + "/blockchain/event");
    assertThat(lastPut.getValue().getEntity()).isEqualTo(event);
  }

  @Test
  public void put() {
    WebClientTest client = setupWebClient();
    Signature dummySig = new Signature(0, BigInteger.ONE, BigInteger.TEN);
    SignatureProvider signatureProvider = hash -> dummySig;
    blockchainClient = new BlockchainClient(baseUrl, 0, client);

    Transaction interactWithContractTransaction =
        Transaction.create(TestObjects.CONTRACT_ONE, new byte[32]);

    BlockchainAddress address =
        BlockchainAddress.fromString("006810f243932574973f750f29e23637588084b95b");

    final long before = System.currentTimeMillis();
    LongSupplier now = System::currentTimeMillis;
    long nonce = ((BlockchainContractClient) blockchainClient).getAccountState(address).nonce();
    String chainId = ((BlockchainContractClient) blockchainClient).getChainId().chainId();
    long validToTime = now.getAsLong() + VALIDITY_PERIOD;

    SignedTransaction signedTransaction =
        SignedTransaction.create(
            signatureProvider, nonce, validToTime, COST, chainId, interactWithContractTransaction);
    blockchainClient.putTransaction(signedTransaction);

    assertThat(client.putUrl).isEqualTo(baseUrl + "/blockchain/transaction");
    Object putObject = client.putObject;
    assertThat(putObject).isInstanceOf(IncomingTransaction.class);
    IncomingTransaction incomingTransaction = (IncomingTransaction) putObject;

    SignedTransaction actual =
        SignedTransaction.read(
            blockchainClient.getChainId().chainId(),
            SafeDataInputStream.createFromBytes(incomingTransaction.transactionPayload()));
    assertThat(actual.getSignature()).isEqualTo(dummySig);
    assertThat(actual.getNonce()).isEqualTo(client.accountState.nonce());
    assertThat(actual.getValidToTime()).isGreaterThanOrEqualTo(before + VALIDITY_PERIOD);
  }

  /**
   * Create a dummy client.
   *
   * @return the dummy client
   */
  public static BlockchainContractClient createDummyBlockchainContractClient() {
    return new BlockchainClient(baseUrl, 0, setupWebClient());
  }

  static WebClientTest setupWebClient() {
    AccountState accountState = new AccountState(42);

    long blockTime = 4342343;
    long productionTime = 144334;
    String identifier = "80631400414909cf7aa120ce0f09c08a54d0851304f5af173a6292aae640d722";
    BlockState block =
        new BlockState(
            "", 0, blockTime, productionTime, List.of(), List.of(), identifier, "", (short) 0);

    ChainId chainId = new ChainId("AwesomeChain");
    return new WebClientTest(chainId, block, accountState);
  }

  @SuppressWarnings("unchecked")
  private static final class WebClientTest implements WebClient {

    private Object putObject;
    private String putUrl;
    private final ChainId chainId;
    private final BlockState latestBlock;
    private final AccountState accountState;

    private WebClientTest(ChainId chainId, BlockState latestBlock, AccountState accountState) {
      this.chainId = chainId;
      this.latestBlock = latestBlock;
      this.accountState = accountState;
    }

    @Override
    public void put(String url, Object entity) {
      putUrl = url;
      putObject = entity;
    }

    @Override
    public <T> T put(String url, Object entity, Class<T> type) {
      return null;
    }

    @Override
    public <RequestT, ResponseT> ResponseT post(
        String url,
        RequestT payload,
        Class<ResponseT> responseClazz,
        Map<String, Object> templates) {
      throw new UnsupportedOperationException();
    }

    @Override
    public <T> T get(
        String url,
        Map<String, Object> queryParams,
        Class<T> clazz,
        Map<String, Object> templates) {
      if (url.contains("/blocks/latest")) {
        return (T) latestBlock;
      } else if (url.contains("/account/")) {
        return (T) accountState;
      } else if (url.contains("/chainId")) {
        return (T) chainId;
      } else if (url.contains("/chain/accounts/")) {
        return (T) JsonNodeFactory.instance.objectNode().put("shardId", "Shard0");
      }
      return null;
    }

    @Override
    public <T> T get(
        String url,
        Map<String, Object> queryParams,
        GenericType<T> type,
        Map<String, Object> templates) {
      return null;
    }
  }
}
