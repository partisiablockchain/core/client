package com.partisiablockchain.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.partisiablockchain.client.web.JerseyWebClient;
import com.partisiablockchain.dto.traversal.TraversePath;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class JerseyWebClientTest extends AbstractJerseyTest {

  private JerseyWebClient client;
  private String url;

  @Override
  @BeforeEach
  public void setUp() {
    super.setUp();
    client = new JerseyWebClient(mocked);
    url = "https://example.com/path";
  }

  @Test
  public void get() {
    expectedResult = new TextNode("Result of get");
    assertThat(client.get(url, JsonNode.class).toString()).isEqualTo("\"Result of get\"");
    assertThat(urlCaptor.getValue()).isEqualTo(url);
    assertThat(lastGet.getValue()).isEqualTo(JsonNode.class);
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getGenericType() {
    expectedResult = List.of(new TextNode("list of text node"));
    List<TextNode> nodes = client.get(url, new GenericType<>() {});
    assertThat(nodes).isNotNull().hasSize(1);
    assertThat(nodes.get(0).toString()).isEqualTo("\"list of text node\"");
    assertThat(urlCaptor.getValue()).isEqualTo(url);
    assertThat(queryParamKeyCaptor.getAllValues()).isEmpty();
    assertThat(queryParamValueCaptor.getAllValues()).isEmpty();
    assertThat(lastGenericGet.getValue()).isEqualTo(new GenericType<List<TextNode>>() {});
    assertThat(lastGet.getAllValues()).isEmpty();
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void getGenericTypeWithQueryParams() {
    expectedResult = List.of(new TextNode("list of text node"));
    List<TextNode> nodes = client.get(url, Map.of("foo", "bar"), new GenericType<>() {}, Map.of());
    assertThat(nodes).isNotNull().hasSize(1);
    assertThat(nodes.get(0).toString()).isEqualTo("\"list of text node\"");
    assertThat(urlCaptor.getValue()).isEqualTo(url);
    assertThat(queryParamKeyCaptor.getValue()).isEqualTo("foo");
    assertThat(queryParamValueCaptor.getValue()).isEqualTo("bar");
    assertThat(lastGenericGet.getValue()).isEqualTo(new GenericType<List<TextNode>>() {});
    assertThat(lastGet.getAllValues()).isEmpty();
    assertThat(lastPut.getAllValues()).isEmpty();
  }

  @Test
  public void put() {
    client.put(url, String.class);
    assertThat(urlCaptor.getValue()).isEqualTo(url);
    assertThat(lastGet.getAllValues()).isEmpty();
    assertThat(lastPut.getValue()).isInstanceOf(Entity.class);

    Entity<?> value = lastPut.getValue();
    Object entity = value.getEntity();
    assertThat(entity).isSameAs(String.class);
  }

  @Test
  public void putFailing() {
    putResult = Response.status(Response.Status.BAD_REQUEST).build();
    client = new JerseyWebClient(mocked);
    assertThatThrownBy(() -> client.put(url, String.class))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("400: Bad Request");
  }

  @Test
  public void putWithReturn() {
    expectedResult = new TextNode("Result of put");
    JsonNode put = client.put(url, 10, JsonNode.class);
    assertThat(put.asText()).isEqualTo("Result of put");
    assertThat(urlCaptor.getValue()).isEqualTo(url);
    assertThat(lastGet.getAllValues()).isEmpty();
    Entity<?> value = lastPut.getValue();
    assertThat(value).isNotNull();
    Object entity = value.getEntity();
    assertThat(entity).isSameAs(10);
  }

  @Test
  public void post() {
    TraversePath payload = new TraversePath(null);
    expectedResult = new TextNode("Result of post");
    JsonNode post = client.post(url, payload, JsonNode.class);
    assertThat(post.asText()).isEqualTo("Result of post");
    assertThat(urlCaptor.getValue()).isEqualTo(url);
    assertThat(lastGet.getAllValues()).isEmpty();
    Entity<?> value = lastPost.getValue();
    assertThat(value).isNotNull();
    Object entity = value.getEntity();
    assertThat(entity).isSameAs(payload);
  }
}
