package com.partisiablockchain.client.shards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.partisiablockchain.BlockchainAddress;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Tests for ShardManagerImpl. */
public final class ShardManagerTest {

  @Test
  public void getGovernanceShard() {
    ShardManager shardManager1 = new ShardManager(0);
    assertThat(shardManager1.getGovernanceShard()).isEqualTo(new ShardId(null));

    ShardManager shardManager2 = new ShardManager(10);
    assertThat(shardManager2.getGovernanceShard()).isEqualTo(new ShardId(null));
  }

  @Test
  public void getShards() {
    ShardManager shardManager1 = new ShardManager(0);
    assertThat(shardManager1.getShards()).hasSize(1);
    assertThat(shardManager1.getShards().get(0)).isEqualTo(shardManager1.getGovernanceShard());

    ShardManager shardManager2 = new ShardManager(10);
    List<ShardId> shards = shardManager2.getShards();
    assertThat(shards).hasSize(11);
    for (int i = 0; i < shards.size() - 1; i++) {
      assertThat(shards.get(i).name()).isEqualTo("Shard" + i);
    }
    // Last index should hold Gov shard
    assertThat(shards.get(10)).isEqualTo(shardManager1.getGovernanceShard());
  }

  @Test
  public void findShardIndex() {
    ShardManager shardManager1 = new ShardManager(0);
    String someAddress = "014242424242424242424242424242424242424242";
    assertThat(shardManager1.findShardIndex(BlockchainAddress.fromString(someAddress)))
        .isEqualTo(0);

    ShardManager shardManager2 = new ShardManager(3);
    String addressMappingToIndex2 = "014242424242424242424242424242424242424241";
    assertThat(shardManager2.findShardIndex(BlockchainAddress.fromString(addressMappingToIndex2)))
        .isEqualTo(2);
  }

  @Test
  public void negativeNumberOfShardsNotAllowed() {
    assertThatCode(() -> new ShardManager(-1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Number of shards must not be negative!");
  }
}
