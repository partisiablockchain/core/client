package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClientTest;
import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.client.web.JerseyWebClient;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.dto.IncomingTransaction;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.rest.RestResources;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.GenericType;
import java.io.File;
import java.io.IOException;
import java.io.Serial;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.bouncycastle.util.encoders.Hex;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.jupiter.api.Test;

/**
 * Tests that sending transactions using {@link BlockchainTransactionClient} works on a real
 * blockchain.
 *
 * <p>This is achieved by running the transaction client on top of a mocked blockchain client that
 * replays recorded responses that were created by interacting with a real blockchain. The
 * recordings of the real calls are then matched against test calls, by testing both request and
 * response for equality.
 *
 * <p>The recordings can be found under test resources in 'blockchain-outputs' for further
 * inspection. New recordings can be made by setting the RECORD_FROM_TESTNET constant to true.
 */
public final class BlockchainTransactionClientFlowTest {

  /** Set to true to record interaction with testnet. */
  private static final boolean RECORD_FROM_TESTNET = false;

  @Test
  public void sendTransaction() {
    final String privateKey = "aa";
    final SenderAuthentication sender = SenderAuthenticationKeyPair.fromString(privateKey);

    BlockchainTransactionClient txClient =
        RECORD_FROM_TESTNET ? createRecordingClient(sender) : createReplayingClient(sender);

    final BlockchainAddress byocFaucetAddress =
        BlockchainAddress.fromString("01860476afca938871ff2c49bf5490235445942e3e");
    final String recipient = "00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed";
    final byte[] rpc = SafeDataOutputStream.serialize(s -> s.write(Hex.decode(recipient)));
    final Transaction transaction = Transaction.create(byocFaucetAddress, rpc);
    final SentTransaction sentTransaction = txClient.signAndSend(transaction, 5910);
    final ExecutedTransactionTree transactionResult =
        txClient.waitForSpawnedEvents(sentTransaction);

    if (RECORD_FROM_TESTNET) {
      // Record now-supplier
      recordNow(sentTransaction);
    }

    assertThat(transactionResult.executedTransaction().executionSucceeded()).isTrue();
    for (ExecutedTransaction event : transactionResult.executedEvents()) {
      assertThat(event.executionSucceeded()).isTrue();
    }
  }

  private static BlockchainTransactionClient createReplayingClient(SenderAuthentication sender) {
    BlockchainTransactionClient txClient;
    WebClient webClientReplay = new WebClientReplaying();
    BlockchainContractClient client =
        BlockchainClientTest.create(
            "https://node1.testnet.partisiablockchain.com", 3, webClientReplay);
    Long now = replayNow();
    ConditionWaiterTestStub conditionWaiter = new ConditionWaiterTestStub();
    txClient =
        BlockchainTransactionClient.createForTest(
            client,
            sender,
            () -> now,
            conditionWaiter,
            BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
    return txClient;
  }

  private static BlockchainTransactionClient createRecordingClient(SenderAuthentication sender) {
    BlockchainTransactionClient txClient;
    Client jakartaClient =
        ClientBuilder.newBuilder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .withConfig(new ResourceConfig().register(RestResources.DEFAULT))
            .build();

    JerseyWebClient jerseyWebClient = new JerseyWebClient(jakartaClient);
    WebClient webClientRecording = new WebClientRecording(jerseyWebClient);
    BlockchainContractClient client =
        BlockchainClientTest.create(
            "https://node1.testnet.partisiablockchain.com", 3, webClientRecording);
    txClient =
        BlockchainTransactionClient.createForTest(
            client,
            sender,
            System::currentTimeMillis,
            ConditionWaiterImpl.create(),
            BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
    return txClient;
  }

  private static void recordNow(SentTransaction sentTransaction) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      File target = Paths.get("src/test/resources/blockchain-outputs/now.json").toFile();
      long nowSupplier =
          sentTransaction.signedTransaction().getValidToTime()
              - BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION;
      objectMapper.writeValue(target, nowSupplier);
    } catch (IOException e) {
      throw new RuntimeException("Failed to write now-supplier", e);
    }
  }

  private static Long replayNow() {
    File target = Paths.get("src/test/resources/blockchain-outputs/now.json").toFile();
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.readValue(target, Long.class);
    } catch (IOException e) {
      throw new RuntimeException("Failed to read now-supplier at: " + target.getAbsolutePath(), e);
    }
  }

  /** Targets real chain. */
  private static final class WebClientRecording implements WebClient {

    private final WebClient client;
    private int index;

    private WebClientRecording(WebClient client) {
      this.client = client;
    }

    @Override
    public <T> T get(String url, Class<T> clazz, Map<String, Object> templates) {
      T output = client.get(url, clazz, templates);
      Get get = new Get(new GetRequest(url, clazz.getName(), templates), output);

      ObjectMapper objectMapper = new ObjectMapper();
      File target =
          Paths.get("src/test/resources/blockchain-outputs/request-response-" + index + ".json")
              .toFile();
      try {
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(target, get);
      } catch (IOException e) {
        throw new RuntimeException("Failed to write" + target.getAbsolutePath(), e);
      }
      index++;
      return output;
    }

    @Override
    public <T> T get(
        String url,
        Map<String, Object> queryParams,
        Class<T> clazz,
        Map<String, Object> templates) {
      return null;
    }

    @Override
    public <T> T get(
        String url,
        Map<String, Object> queryParams,
        GenericType<T> type,
        Map<String, Object> templates) {
      return null;
    }

    @Override
    public void put(String url, Object entity) {
      Put put = new Put(url, "com.partisiablockchain.dto.IncomingTransaction", entity);
      ObjectMapper objectMapper = new ObjectMapper();
      File target =
          Paths.get("src/test/resources/blockchain-outputs/request-response-" + index + ".json")
              .toFile();
      try {
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(target, put);
      } catch (IOException e) {
        throw new RuntimeException("Failed to write" + target.getAbsolutePath(), e);
      }
      index++;

      client.put(url, entity);
    }

    @Override
    public <T> T put(String url, Object entity, Class<T> type) {
      return null;
    }

    @Override
    public <RequestT, ResponseT> ResponseT post(
        String url,
        RequestT payload,
        Class<ResponseT> responseClazz,
        Map<String, Object> templates) {
      return null;
    }
  }

  /** Targets mock chain. */
  private static final class WebClientReplaying implements WebClient {
    private int index = 0;
    private final ObjectMapper jsonObjectMapper = getJsonObjectMapper();

    public WebClientReplaying() {}

    @Override
    public <T> T get(String url, Class<T> clazz, Map<String, Object> templates) {
      RequestResponse requestResponse = getNextRequestResponse();
      assertThat(requestResponse).isInstanceOf(Get.class);
      Get recordedGet = (Get) requestResponse;
      assertThat(url)
          .withFailMessage(
              "\n Url did not match. \n Expected: %s \n But was: %s",
              recordedGet.request.toString(), new GetRequest(url, clazz.getName(), templates))
          .isEqualTo(recordedGet.request.url);
      assertThat(clazz.getName())
          .withFailMessage(
              "\n Class did not match. \n Expected: %s \n But was: %s",
              recordedGet.request.toString(), new GetRequest(url, clazz.getName(), templates))
          .isEqualTo(recordedGet.request.className);
      assertThat(templates)
          .withFailMessage(
              "\n Templates did not match. \n Expected: %s \n But was: %s",
              recordedGet.request.toString(), new GetRequest(url, clazz.getName(), templates))
          .isEqualTo(recordedGet.request.templates);
      @SuppressWarnings("unchecked")
      T output = (T) recordedGet.response;
      return output;
    }

    @Override
    public <T> T get(
        String url,
        Map<String, Object> queryParams,
        Class<T> clazz,
        Map<String, Object> templates) {
      return null;
    }

    @Override
    public <T> T get(
        String url,
        Map<String, Object> queryParams,
        GenericType<T> type,
        Map<String, Object> templates) {
      return null;
    }

    @Override
    public void put(String url, Object entity) {
      RequestResponse requestResponse = getNextRequestResponse();
      assertThat(requestResponse).isInstanceOf(Put.class);
      Put recordedPut = (Put) requestResponse;
      assertThat(url).isEqualTo(recordedPut.url);

      IncomingTransaction e1 = (IncomingTransaction) entity;
      IncomingTransaction e2 = (IncomingTransaction) recordedPut.entity;

      ChainId chainId = parseChainId();

      SignedTransaction st1 =
          SafeDataInputStream.deserialize(
              safeDataInputStream -> SignedTransaction.read(chainId.chainId(), safeDataInputStream),
              e1.transactionPayload());

      SignedTransaction st2 =
          SafeDataInputStream.deserialize(
              safeDataInputStream -> SignedTransaction.read(chainId.chainId(), safeDataInputStream),
              e2.transactionPayload());

      assertThat(st1).isEqualTo(st2);
      assertThat(e1.transactionPayload()).isEqualTo(e2.transactionPayload());
    }

    @Override
    public <T> T put(String url, Object entity, Class<T> type) {
      return null;
    }

    @Override
    public <RequestT, ResponseT> ResponseT post(
        String url,
        RequestT payload,
        Class<ResponseT> responseClazz,
        Map<String, Object> templates) {
      return null;
    }

    private RequestResponse getNextRequestResponse() {
      RequestResponse recordedRequestResponse;
      File jsonFile =
          Paths.get("src/test/resources/blockchain-outputs/request-response-" + index + ".json")
              .toFile();
      try {
        recordedRequestResponse = jsonObjectMapper.readValue(jsonFile, RequestResponse.class);
      } catch (Throwable e) {
        throw new RuntimeException("Cannot deserialize " + jsonFile.getAbsolutePath(), e);
      }
      System.out.printf(
          "Replaying blockchain interaction %d:\n %s \n\n", index, recordedRequestResponse);
      index++;
      return recordedRequestResponse;
    }

    private ChainId parseChainId() {
      RequestResponse getRequest;
      File jsonFile =
          Paths.get("src/test/resources/blockchain-outputs/request-response-1.json").toFile();
      try {
        getRequest = jsonObjectMapper.readValue(jsonFile, RequestResponse.class);
      } catch (Throwable e) {
        throw new RuntimeException("Cannot deserialize " + jsonFile.getAbsolutePath(), e);
      }
      assertThat(getRequest).isInstanceOf(Get.class);
      Get get = (Get) getRequest;
      Object response = get.response;
      assertThat(response).isInstanceOf(ChainId.class);
      return (ChainId) response;
    }
  }

  private static ObjectMapper getJsonObjectMapper() {
    ObjectMapper mapper = new ObjectMapper();
    SimpleModule module = new SimpleModule();
    module.addDeserializer(Get.class, new GetJsonDeserializer());
    module.addDeserializer(Put.class, new PutJsonDeserializer());
    mapper.registerModule(module);
    return mapper;
  }

  private static final class GetJsonDeserializer extends StdDeserializer<Get> {
    @Serial private static final long serialVersionUID = 1234567L;

    public GetJsonDeserializer() {
      super(Get.class);
    }

    @Override
    public Get deserialize(JsonParser jp, DeserializationContext ctx) throws IOException {
      TreeNode node = jp.getCodec().readTree(jp);
      JsonParser requestParser = node.get("request").traverse();
      requestParser.nextToken();
      GetRequest getRequest = ctx.readValue(requestParser, GetRequest.class);
      Class<?> responseClazz;
      try {
        responseClazz = this.getClass().getClassLoader().loadClass(getRequest.className);
      } catch (ClassNotFoundException e) {
        throw new RuntimeException("Class not found " + getRequest.className, e);
      }
      JsonParser responseParser = node.get("response").traverse();
      responseParser.nextToken();
      Object response = ctx.readValue(responseParser, responseClazz);
      return new Get(getRequest, response);
    }
  }

  private static final class PutJsonDeserializer extends StdDeserializer<Put> {
    @Serial private static final long serialVersionUID = 1234568L;

    public PutJsonDeserializer() {
      super(Put.class);
    }

    @Override
    public Put deserialize(JsonParser jp, DeserializationContext ctx) throws IOException {
      TreeNode node = jp.getCodec().readTree(jp);

      JsonParser urlParser = node.get("url").traverse();
      urlParser.nextToken();
      final String url = ctx.readValue(urlParser, String.class);

      JsonParser classNameParser = node.get("entityClassName").traverse();
      classNameParser.nextToken();
      final String className = ctx.readValue(classNameParser, String.class);

      Class<?> entityClass;
      try {
        entityClass = this.getClass().getClassLoader().loadClass(className);
      } catch (ClassNotFoundException e) {
        throw new RuntimeException("Class not found " + className, e);
      }

      JsonParser entityParser = node.get("entity").traverse();
      entityParser.nextToken();
      final Object entity = ctx.readValue(entityParser, entityClass);

      return new Put(url, className, entity);
    }
  }

  @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
  @JsonSubTypes({
    @JsonSubTypes.Type(value = Get.class, name = "get"),
    @JsonSubTypes.Type(value = Put.class, name = "put"),
  })
  private interface RequestResponse {}

  @SuppressWarnings("unused")
  private record Get(GetRequest request, Object response) implements RequestResponse {}

  @SuppressWarnings("unused")
  private record Put(String url, String entityClassName, Object entity)
      implements RequestResponse {}

  @SuppressWarnings("unused")
  private record GetRequest(String url, String className, Map<String, Object> templates) {}
}
