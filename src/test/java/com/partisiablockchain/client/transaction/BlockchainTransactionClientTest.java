package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.client.BlockchainClientTest;
import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.AccountState;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.dto.Event;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.dto.TransactionPointer;
import jakarta.ws.rs.NotFoundException;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.function.Supplier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/** Tests for BlockchainTransactionClient. */
public final class BlockchainTransactionClientTest {

  /**
   * Tests that we wait for transactions according to their validity duration. Waits for a
   * transaction that never arrives. The time waited should correspond to the validity duration of a
   * transaction, regardless of current time.
   */
  @Test
  public void testWaitTimeInWaitForInclusionInBlock() {
    long startTime = 1_000_000L;
    ConditionWaiterTestStub timeSupport = new ConditionWaiterTestStub();
    BlockchainTransactionClient client =
        BlockchainTransactionClient.createForTest(
            new GetInputTransactionClient(null),
            SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY),
            () -> startTime,
            timeSupport,
            BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);
    SentTransaction sentTransaction = client.signAndSend(transaction, 1);
    assertThat(sentTransaction.signedTransaction().getValidToTime())
        .isEqualTo(startTime + BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION);
    client.waitForInclusionInBlock(sentTransaction);

    timeSupport.assertHasWaitedWithoutResult(
        BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION);
  }

  /**
   * Tests that the time waited for a transaction is only dependent on the validity duration at the
   * time of creation. As such, we expect to wait a total of 3 minutes with the default duration
   * regardless of changes to the transaction validity duration, post signing.
   */
  @Test
  void transactionsAreAffectedByValidityDuration() {
    long startTime = 1_000_000L;
    ConditionWaiterTestStub timeSupport = new ConditionWaiterTestStub();
    BlockchainTransactionClient client =
        BlockchainTransactionClient.createForTest(
            new GetInputTransactionClient(null),
            SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY),
            () -> startTime,
            timeSupport,
            Integer.MIN_VALUE,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);
    SentTransaction sentTransaction = client.signAndSend(transaction, 1);

    // Set new validity duration for future transactions
    client.waitForInclusionInBlock(sentTransaction);

    timeSupport.assertHasWaitedWithoutResult(Integer.MIN_VALUE);
  }

  @Test
  public void create() {
    BlockchainContractClient blockchainContractClient =
        BlockchainClientTest.createDummyBlockchainContractClient();
    SenderAuthentication senderAuthentication =
        SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY);

    BlockchainTransactionClient control =
        BlockchainTransactionClient.createForTest(
            blockchainContractClient,
            senderAuthentication,
            System::currentTimeMillis,
            ConditionWaiterImpl.create(),
            BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
    BlockchainTransactionClient created =
        BlockchainTransactionClient.create(blockchainContractClient, senderAuthentication);

    assertThat(control).usingRecursiveComparison().isEqualTo(created);
  }

  @Test
  public void waitForEmptyEvents() {
    ExecutedTransaction transactionWithoutInnerEvent =
        new ExecutedTransaction(
            null, null, 0, 0, null, null, true, List.of(), false, false, null, null);
    BlockchainContractClient blockchainContractClient =
        new GetInputTransactionClient(transactionWithoutInnerEvent);
    SenderAuthentication senderAuthentication =
        SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY);
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.createForTest(
            blockchainContractClient,
            senderAuthentication,
            () -> 0,
            new ConditionWaiterTestStub(),
            100,
            100);

    ExecutedTransaction executedTransaction =
        new ExecutedTransaction(
            null, null, 0, 0, null, null, true, List.of(), false, false, null, null);

    ExecutedTransactionTree executedTransactionTree =
        transactionClient.waitForSpawnedEvents(executedTransaction);
    assertThat(executedTransactionTree.executedEvents().isEmpty()).isTrue();
  }

  @Test
  public void waitForEvents() {
    ExecutedTransaction transactionWithoutInnerEvent =
        new ExecutedTransaction(
            null, null, 0, 0, null, null, true, List.of(), false, false, null, null);

    Event innerEvent =
        new Event("abababababababababababababababababababababababababababababababab", "Shard0");
    ExecutedTransaction transactionWithInnerEvent =
        new ExecutedTransaction(
            null, null, 0, 0, null, null, true, List.of(innerEvent), false, false, null, null);

    BlockchainContractClient blockchainContractClient =
        new GetInputTransactionClient(transactionWithoutInnerEvent);
    SenderAuthentication senderAuthentication =
        SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY);
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.createForTest(
            blockchainContractClient,
            senderAuthentication,
            () -> 0,
            new ConditionWaiterTestStub(),
            100,
            100);

    ExecutedTransactionTree executedTransactionTree =
        transactionClient.waitForSpawnedEvents(transactionWithInnerEvent);
    assertThat(executedTransactionTree.executedEvents().isEmpty()).isFalse();
    assertThat(executedTransactionTree.executedEvents().get(0))
        .isEqualTo(transactionWithoutInnerEvent);
  }

  @Test
  public void transactionValidityShouldBe123Milliseconds() {
    BlockchainContractClient blockchainContractClient =
        BlockchainClientTest.createDummyBlockchainContractClient();
    SenderAuthentication senderAuthentication =
        SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY);
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.createForTest(
            blockchainContractClient,
            senderAuthentication,
            () -> 0,
            new ConditionWaiterTestStub(),
            123,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);

    SignedTransaction signedTransaction =
        transactionClient.sign(Transaction.create(TestObjects.CONTRACT_ONE, new byte[] {0}), 0);
    assertThat(signedTransaction.getValidToTime()).isEqualTo(123);
  }

  @Test
  public void errorInTxLookupGivesStdError() {
    Event innerEvent =
        new Event("abababababababababababababababababababababababababababababababab", "Shard0");
    ExecutedTransaction transactionWithInnerEvent =
        new ExecutedTransaction(
            null, null, 0, 0, null, null, true, List.of(innerEvent), false, false, null, null);

    BlockchainContractClient blockchainContractClient = new GetInputTransactionClient(null);
    SenderAuthentication senderAuthentication =
        SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY);
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.createForTest(
            blockchainContractClient,
            senderAuthentication,
            () -> 0,
            throwingConditionWaiter(),
            BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
            1);

    assertThatThrownBy(() -> transactionClient.waitForSpawnedEvents(transactionWithInnerEvent))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("was not included in a block at shard")
        .hasMessageContaining(innerEvent.identifier());
  }

  private ConditionWaiter throwingConditionWaiter() {
    return new ConditionWaiter() {
      @Override
      public <T> T waitForCondition(
          Supplier<T> valueSupplier,
          Predicate<T> valuePredicate,
          long timeoutMs,
          Supplier<String> errorMessage) {
        throw new RuntimeException(errorMessage.get());
      }
    };
  }

  private ConditionWaiter getOrAssertTimeoutConditionWaiter(long timeout) {
    return new ConditionWaiter() {
      @Override
      public <T> T waitForCondition(
          Supplier<T> valueSupplier,
          Predicate<T> valuePredicate,
          long timeoutMs,
          Supplier<String> errorMessage) {
        T value = valueSupplier.get();
        if (valuePredicate.test(value)) {
          return value;
        } else {
          assertThat(timeoutMs).isEqualTo(timeout);
          throw new RuntimeException(errorMessage.get());
        }
      }
    };
  }

  private ConditionWaiter getOrThrowConditionWaiter() {
    return new ConditionWaiter() {
      @Override
      public <T> T waitForCondition(
          Supplier<T> valueSupplier,
          Predicate<T> valuePredicate,
          long timeoutMs,
          Supplier<String> errorMessage) {
        T value = valueSupplier.get();
        if (valuePredicate.test(value)) {
          return value;
        } else {
          throw new RuntimeException(errorMessage.get());
        }
      }
    };
  }

  @DisplayName("Will only try to resend transactions until include before.")
  @Test
  void willOnlyRetrySendingTransactionUntilIncludeBefore() {
    final long startTime = 1_000_000L;
    AtomicInteger timeSupportCalls = new AtomicInteger(0);
    BlockchainTransactionClient.CurrentTime customTime =
        () -> {
          int numberOfCalls = timeSupportCalls.incrementAndGet();
          if (numberOfCalls <= 3) { // First round
            return startTime;
          } else if (numberOfCalls <= 5) { // second round
            return startTime + 500;
          } else {
            throw new RuntimeException("Should not happen!");
          }
        };
    Instant before = Instant.ofEpochMilli(startTime + 500);
    BlockchainTransactionClient client =
        setupTransactionClient(
            new GetInputTransactionClient(null, null),
            customTime,
            getOrAssertTimeoutConditionWaiter(500));
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    assertThatThrownBy(() -> client.ensureInclusionInBlock(transaction, 1, before))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unable to push transaction to chain");
  }

  /** A signed and sent transaction is included in a block immediately. */
  @Test
  void transactionIsIncludedImmediately() {
    ExecutedTransaction expectedTransaction =
        new ExecutedTransaction(
            null, null, 0, 0, null, null, true, List.of(), false, false, null, null);
    long startTime = 1_000_000L;
    BlockchainTransactionClient txclient =
        setupTransactionClient(
            new GetInputTransactionClient(expectedTransaction),
            () -> startTime,
            getOrThrowConditionWaiter());
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    ExecutedTransaction actualTransaction =
        txclient.ensureInclusionInBlock(transaction, 1, Instant.ofEpochMilli(startTime + 1));
    assertThat(actualTransaction).isEqualTo(expectedTransaction);
  }

  /**
   * We wait for a transaction to be included in a block that is slow to be finalized, before
   * retrying.
   */
  @Test
  void transactionIsIncludedInSlowBlock() {
    ExecutedTransaction expectedTransaction =
        new ExecutedTransaction(
            null, null, 0, 0, null, null, true, List.of(), false, false, null, null);
    long startTime = 1_000_000L;
    long beforeTime = startTime + 500L;
    AtomicInteger waitConditionCalls = new AtomicInteger(0);
    GetInputTransactionClient innerClient = new GetInputTransactionClient(expectedTransaction);
    ConditionWaiter customWaiter =
        new ConditionWaiter() {
          @Override
          public <T> T waitForCondition(
              Supplier<T> valueSupplier,
              Predicate<T> valuePredicate,
              long timeoutMs,
              Supplier<String> errorMessage) {
            int numberOfCalls = waitConditionCalls.incrementAndGet();
            if (numberOfCalls == 1) { // signAndSend transaction
              return valueSupplier.get();
            } else if (numberOfCalls == 2) { // Waiting for transaction
              throw new RuntimeException(errorMessage.get());
            } else if (numberOfCalls == 3) { // Waiting for latest block
              innerClient.setProductionTimeOfLatestFinalBlock(beforeTime - 1);
              T blockState = valueSupplier.get();
              assertThat(valuePredicate.test(blockState)).isFalse();

              innerClient.setProductionTimeOfLatestFinalBlock(beforeTime);
              blockState = valueSupplier.get();
              assertThat(valuePredicate.test(blockState)).isFalse();

              innerClient.setProductionTimeOfLatestFinalBlock(beforeTime + 1);
              blockState = valueSupplier.get();
              assertThat(valuePredicate.test(blockState)).isTrue();

              return valueSupplier.get();
            } else {
              throw new RuntimeException("Should not have happened!");
            }
          }
        };
    BlockchainTransactionClient client =
        setupTransactionClient(innerClient, () -> startTime, customWaiter);
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    ExecutedTransaction actualTransaction =
        client.ensureInclusionInBlock(transaction, 1, Instant.ofEpochMilli(beforeTime));
    assertThat(actualTransaction).isEqualTo(expectedTransaction);
  }

  /** A transaction is re-signed and sent if first attempt to include fails. */
  @Test
  void transactionInclusionIsRetried() {
    ExecutedTransaction expectedTransaction =
        new ExecutedTransaction(
            null, null, 0, 0, null, null, true, List.of(), false, false, null, null);
    long startTime = 1_000_000L;
    long beforeTime = startTime + 500L;
    long blockWaitTime = 600_000;
    AtomicInteger waitConditionCalls = new AtomicInteger(0);
    GetInputTransactionClient innerClient = new GetInputTransactionClient(null);
    ConditionWaiter customWaiter =
        new ConditionWaiter() {
          @Override
          public <T> T waitForCondition(
              Supplier<T> valueSupplier,
              Predicate<T> valuePredicate,
              long timeoutMs,
              Supplier<String> errorMessage) {
            int numberOfCalls = waitConditionCalls.incrementAndGet();
            if (numberOfCalls == 1) { // signAndSend transaction first time
              return valueSupplier.get();
            } else if (numberOfCalls == 2) { // Waiting for transaction
              throw new RuntimeException(errorMessage.get());
            } else if (numberOfCalls == 3) { // Waiting for latest block
              assertThat(timeoutMs).isEqualTo(beforeTime + 600_000);
              return valueSupplier.get();
            } else if (numberOfCalls == 4) { // signAndSend transaction second time
              return valueSupplier.get();
            } else if (numberOfCalls == 5) { // Waiting for transaction
              innerClient.setTransaction(expectedTransaction);
              return valueSupplier.get();
            } else {
              throw new RuntimeException("Should not have happened!");
            }
          }
        };
    BlockchainTransactionClient client =
        setupTransactionClient(innerClient, () -> startTime, customWaiter);
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    ExecutedTransaction actualTransaction =
        client.ensureInclusionInBlock(
            transaction, 1, Instant.ofEpochMilli(beforeTime), Duration.ofMillis(blockWaitTime));
    assertThat(actualTransaction).isEqualTo(expectedTransaction);
  }

  /** If we are unable to get latest block before timeout we do not retry sending again. */
  @Test
  void latestBlockFailureAbortsResending() {
    long startTime = 1_000_000L;
    BlockchainTransactionClient client =
        setupTransactionClient(
            new GetInputTransactionClient(null), () -> startTime, getOrThrowConditionWaiter());
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    Instant before = Instant.ofEpochMilli(startTime + 500);
    assertThatThrownBy(() -> client.ensureInclusionInBlock(transaction, 1, before))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Unable to get block with production time greater than 1000500 for shard"
                + " ShardId[name=]");
  }

  /** If the applicable until time limit expires, we do not retry sending transaction. */
  @Test
  void applicableUntilExpires() {
    long startTime = 1_000_000L;
    long beforeTime = startTime + 500L;
    AtomicInteger timeSupportCalls = new AtomicInteger(0);
    GetInputTransactionClient innerClient = new GetInputTransactionClient(null);
    innerClient.setProductionTimeOfLatestFinalBlock(beforeTime + 1);
    BlockchainTransactionClient.CurrentTime customTime =
        () -> {
          int numberOfCalls = timeSupportCalls.incrementAndGet();
          if (numberOfCalls <= 3) { // First round
            return startTime;
          } else if (numberOfCalls <= 8) { // second round
            return beforeTime - 1;
          } else if (numberOfCalls == 9) {
            return beforeTime;
          } else {
            throw new RuntimeException("Should not happen!");
          }
        };
    BlockchainTransactionClient client =
        setupTransactionClient(innerClient, customTime, getOrThrowConditionWaiter());
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    assertThatThrownBy(
            () -> client.ensureInclusionInBlock(transaction, 1, Instant.ofEpochMilli(beforeTime)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Transaction was not included in block before 1970-01-01T00:16:40.500Z");
  }

  /** If applicable until time is in the past we do not try to sign and send. */
  @Test
  void applicableUntilTimeInThePast() {
    long startTime = 1_000_000L;
    long beforeTime = startTime + 500L;
    AtomicInteger timeSupportCalls = new AtomicInteger(0);
    BlockchainTransactionClient.CurrentTime customTime =
        () -> {
          int numberOfCalls = timeSupportCalls.incrementAndGet();
          if (numberOfCalls == 1) {
            return beforeTime + 1500;
          } else {
            throw new RuntimeException("Should not happen!");
          }
        };
    BlockchainTransactionClient client =
        setupTransactionClient(
            new GetInputTransactionClient(null), customTime, getOrThrowConditionWaiter());
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    assertThatThrownBy(
            () -> client.ensureInclusionInBlock(transaction, 1, Instant.ofEpochMilli(beforeTime)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Transaction was not included in block before 1970-01-01T00:16:40.500Z");
  }

  @DisplayName("Will continue sending transactions until the blockchain accepts the transaction")
  @Test
  void sendTransactionTwoTimesBeforeGoingThrough() {
    long startTime = 0;
    long beforeTime = startTime + 500L;
    AtomicInteger timeSupportCalls = new AtomicInteger(0);
    GetInputTransactionClient blockchainClient = new GetInputTransactionClient(null, null);
    final ExecutedTransaction expectedTransaction =
        new ExecutedTransaction(
            null, null, 0, 0, null, null, true, List.of(), false, false, null, null);
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);
    SignedTransaction signedTransaction =
        SignedTransaction.create(
            SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY),
            0L,
            500L,
            1L,
            "someChain",
            transaction);
    BlockchainTransactionClient.CurrentTime customTime =
        () -> {
          int numberOfCalls = timeSupportCalls.incrementAndGet();
          if (numberOfCalls == 1) {
            return startTime;
          } else if (numberOfCalls == 6) {
            blockchainClient.addTransactionPointerForTransaction(signedTransaction);
            blockchainClient.setTransaction(expectedTransaction);
            return startTime;
          } else if (numberOfCalls == 7) {
            // Catch if too many calls is made
            throw new RuntimeException("Should not happen!");
          } else {
            return startTime;
          }
        };
    BlockchainTransactionClient client =
        setupTransactionClient(blockchainClient, customTime, new ConditionWaiterTestStub());

    ExecutedTransaction actualTransaction =
        client.ensureInclusionInBlock(transaction, 1, Instant.ofEpochMilli(beforeTime));
    assertThat(actualTransaction).isEqualTo(expectedTransaction);
    assertThat(timeSupportCalls.get()).isEqualTo(7);
  }

  @DisplayName("Sending a failing transaction without retrying results in an exception.")
  @Test
  void sendTransactionWithoutRetrying() {
    final Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);
    BlockchainTransactionClient client =
        setupTransactionClient(
            new GetInputTransactionClient(null, null), () -> 0L, new ConditionWaiterTestStub());
    assertThatThrownBy(() -> client.signAndSend(transaction, 100L))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("The received transaction is invalid in the current state of the blockchain.");
  }

  private BlockchainTransactionClient setupTransactionClient(
      BlockchainContractClient blockchainClient,
      BlockchainTransactionClient.CurrentTime time,
      ConditionWaiter conditionWaiter) {
    return BlockchainTransactionClient.createForTest(
        blockchainClient,
        SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY),
        time,
        conditionWaiter,
        BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
        BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
  }

  private static final class GetInputTransactionClient implements BlockchainContractClient {

    private ExecutedTransaction inputTransaction;
    private TransactionPointer defaultTransactionPointer;
    private HashMap<SignedTransaction, TransactionPointer> transactions;
    private long latestBlockProductionTime;

    public GetInputTransactionClient(ExecutedTransaction inputTransaction) {
      this(inputTransaction, new TransactionPointer(null, ""));
    }

    public GetInputTransactionClient(
        ExecutedTransaction inputTransaction, TransactionPointer transactionPointer) {
      this.inputTransaction = inputTransaction;
      this.defaultTransactionPointer = transactionPointer;
      this.latestBlockProductionTime = 0L;
      this.transactions = new HashMap<>();
    }

    @Override
    public ChainId getChainId() {
      return new ChainId("someChain");
    }

    @Override
    public AccountState getAccountState(BlockchainAddress address) {
      return new AccountState(0);
    }

    @Override
    public ContractState getContractState(BlockchainAddress address) {
      return null;
    }

    public void setTransaction(ExecutedTransaction transaction) {
      this.inputTransaction = transaction;
    }

    @Override
    public ExecutedTransaction getTransaction(ShardId shardId, Hash transactionId) {
      if (inputTransaction == null) {
        throw new NotFoundException("HTTP 404 Not Found");
      }
      return inputTransaction;
    }

    void addTransactionPointerForTransaction(SignedTransaction signedTransaction) {
      transactions.put(
          signedTransaction, new TransactionPointer(Hash.fromString("a".repeat(64)), "Shard0"));
    }

    @Override
    public TransactionPointer putTransaction(SignedTransaction signedTransaction) {
      TransactionPointer transactionPointer =
          transactions.getOrDefault(signedTransaction, defaultTransactionPointer);
      if (transactionPointer == null) {
        throw new RuntimeException(
            "The received transaction is invalid in the current state of the blockchain.");
      }
      return transactionPointer;
    }

    public void setProductionTimeOfLatestFinalBlock(long productionTime) {
      this.latestBlockProductionTime = productionTime;
    }

    @Override
    public BlockState getLatestBlock(ShardId shardId) {
      return new BlockState(
          null, 0L, 0L, latestBlockProductionTime, List.of(), List.of(), null, null, (short) 0);
    }
  }
}
