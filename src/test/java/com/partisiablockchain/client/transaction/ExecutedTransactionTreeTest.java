package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.dto.ExecutedTransaction;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Testing {@link ExecutedTransactionTree}. */
public final class ExecutedTransactionTreeTest {

  /** An executed transaction tree where all transactions and event succeeded returns false. */
  @Test
  void transactionTreeSucceeded() {
    ExecutedTransactionTree succeedingTransactionTree =
        new ExecutedTransactionTree(
            executedTransaction(true),
            List.of(executedTransaction(true), executedTransaction(true)));
    assertThat(succeedingTransactionTree.hasFailures()).isFalse();
  }

  /**
   * An executed transaction tree where either the transaction or one of the events failed returns
   * true.
   */
  @Test
  void transactionTreeFailed() {
    ExecutedTransactionTree failingTransactionTree =
        new ExecutedTransactionTree(executedTransaction(false), List.of());
    assertThat(failingTransactionTree.hasFailures()).isTrue();
    ExecutedTransactionTree failingTransactionTree2 =
        new ExecutedTransactionTree(
            executedTransaction(true),
            List.of(
                executedTransaction(true), executedTransaction(false), executedTransaction(true)));
    assertThat(failingTransactionTree2.hasFailures()).isTrue();
  }

  private ExecutedTransaction executedTransaction(boolean succeeded) {
    return new ExecutedTransaction(
        null, null, 0, 0, null, null, succeeded, null, true, true, null, null);
  }
}
